package com.mgryshenko.samples.spring_boot.task;

import com.mgryshenko.samples.spring_boot.persistence.dao.IJwtRefreshTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.dao.IPasswordResetTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.dao.IVerificationTokenRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Date;

@Service
public class TokenPurgeTask {

    protected final IJwtRefreshTokenRepository refreshTokenRepository;
    protected final IVerificationTokenRepository verificationTokenRepository;
    protected final IPasswordResetTokenRepository resetPasswordTokenRepository;

    public TokenPurgeTask(IJwtRefreshTokenRepository refreshTokenRepository,
                          IVerificationTokenRepository verificationTokenRepository,
                          IPasswordResetTokenRepository resetPasswordTokenRepository) {
        this.resetPasswordTokenRepository = resetPasswordTokenRepository;
        this.refreshTokenRepository = refreshTokenRepository;
        this.verificationTokenRepository = verificationTokenRepository;
    }

    @Scheduled(fixedRate = 5 * 60 * 1000)
    @Transactional
    public void purgeExpired() {
        Date now = Date.from(Instant.now());

        resetPasswordTokenRepository.deleteAllExpiredSince(now);
        refreshTokenRepository.deleteAllExpiredSince(now);
        verificationTokenRepository.deleteAllExpiredSince(now);
    }
}
