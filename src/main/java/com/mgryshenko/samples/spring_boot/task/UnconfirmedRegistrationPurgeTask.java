package com.mgryshenko.samples.spring_boot.task;

import com.mgryshenko.samples.spring_boot.persistence.dao.IVerificationTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.VerificationToken;
import com.mgryshenko.samples.spring_boot.service.IUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Date;

@Service
public class UnconfirmedRegistrationPurgeTask {

    protected final IUserAccountService userAccountService;
    protected final IVerificationTokenRepository verificationTokenRepository;

    @Autowired
    public UnconfirmedRegistrationPurgeTask(IUserAccountService userAccountService,
                                            IVerificationTokenRepository verificationTokenRepository) {
        this.userAccountService = userAccountService;
        this.verificationTokenRepository = verificationTokenRepository;
    }

    @Scheduled(fixedRate = 5 * 60 * 1000)
    @Transactional
    public void purgeExpired() {
        Date expireDate = Date.from(Instant.now());
        Iterable<VerificationToken> expired = verificationTokenRepository.findAllByExpiryDateBefore(expireDate);
        if (expired != null) {
            expired.forEach(token -> {
                userAccountService.deleteUser(token.getUserId());
            });
        }
    }
}
