package com.mgryshenko.samples.spring_boot.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordMatcher.class)
@Documented
public @interface PasswordsMatched {
    String message() default "";
    boolean shouldMatch() default true;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
