package com.mgryshenko.samples.spring_boot.validation;

import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class PasswordStrengthValidator implements ConstraintValidator<PasswordStrong, String> {

    PasswordValidator validator = new PasswordValidator(Arrays.asList(
            new LengthRule(8, 30),
            new UppercaseCharacterRule(1),
//            new DigitCharacterRule(1),
//            new SpecialCharacterRule(1),
//            new NumericalSequenceRule(3, false),
            new AlphabeticalSequenceRule(3, false),
            new QwertySequenceRule(3, false),
            new WhitespaceRule()
    ));

    @Override
    public boolean isValid(String pwd, ConstraintValidatorContext context) {
        if (pwd == null) {
            return false;
        }
        RuleResult result = validator.validate(new PasswordData(pwd));
        if (result.isValid()) {
            return true;
        }

        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(messages(result))
                .addConstraintViolation();
        return false;
    }

    private String messages(RuleResult result) {
        return String.join(",", validator.getMessages(result));
    }

}
