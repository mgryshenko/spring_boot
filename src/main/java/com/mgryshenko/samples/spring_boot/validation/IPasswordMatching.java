package com.mgryshenko.samples.spring_boot.validation;

public interface IPasswordMatching {
    String getPassword();
    String getMatchingPassword();
}
