package com.mgryshenko.samples.spring_boot.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class PasswordMatcher implements ConstraintValidator<PasswordsMatched, IPasswordMatching> {

    private boolean shouldMatch;
    private String message;

    @Override
    public void initialize(PasswordsMatched constraintAnnotation) {
        shouldMatch = constraintAnnotation.shouldMatch();
        if (shouldMatch) {
            message = "Passwords should match";
        } else {
            message = "Passwords shouldn't match";
        }
    }

    @Override
    public boolean isValid(IPasswordMatching data, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        return Objects.equals(data.getPassword(), data.getMatchingPassword()) == shouldMatch;
    }
}
