package com.mgryshenko.samples.spring_boot.persistence.dao;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.IUserTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.VerificationToken;

import java.util.Date;

public interface IVerificationTokenRepository extends IUserTokenRepository<VerificationToken> {
    Iterable<VerificationToken> findAllByExpiryDateBefore(Date now);
}
