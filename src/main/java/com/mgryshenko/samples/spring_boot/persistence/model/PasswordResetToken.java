package com.mgryshenko.samples.spring_boot.persistence.model;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;
import com.mgryshenko.samples.spring_boot.service.token.IExpiryDateTokenDetails;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "password_reset_token")
public class PasswordResetToken extends UserTokenEntity implements IExpiryDateTokenDetails {
    public static PasswordResetToken withId(Long id) {
        PasswordResetToken token = new PasswordResetToken();
        token.setId(id);
        return token;
    }
}
