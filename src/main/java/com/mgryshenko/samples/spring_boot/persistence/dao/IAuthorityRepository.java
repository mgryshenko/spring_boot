package com.mgryshenko.samples.spring_boot.persistence.dao;

import com.mgryshenko.samples.spring_boot.persistence.model.Authority;
import org.springframework.data.repository.CrudRepository;

public interface IAuthorityRepository extends CrudRepository<Authority, Long> {

    Authority findByName(String name);

    @Override
    void delete(Authority authority);
}
