package com.mgryshenko.samples.spring_boot.persistence.dao.token;

import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Date;

@NoRepositoryBean
public interface IUserTokenRepository<T extends UserTokenEntity> extends CrudRepository<T, Long> {
    <R> R findByToken(String token, Class<R> type);

    void deleteAllByUser(UserAccount user);

    @Modifying
    @Query("delete from #{#entityName} t where t.expiryDate <= ?1")
    void deleteAllExpiredSince(Date now);
}