package com.mgryshenko.samples.spring_boot.persistence.model;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;
import com.mgryshenko.samples.spring_boot.service.token.IExpiryDateTokenDetails;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "verification_token")
public class VerificationToken extends UserTokenEntity implements IExpiryDateTokenDetails {
    public static VerificationToken withId(Long id) {
        VerificationToken token = new VerificationToken();
        token.setId(id);
        return token;
    }
}
