package com.mgryshenko.samples.spring_boot.persistence.dao.token;

import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@MappedSuperclass
public abstract class UserTokenEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String token;

    @ManyToOne(targetEntity = UserAccount.class, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "user_id")
    private UserAccount user;

    @Column(name = "user_id", insertable = false, updatable = false)
    private Long userId;

    private Date expiryDate;

    @Override
    public String toString() {
        return "Token{" +
                "id=" + id +
                ", token='" + token + "'" +
                ", userId=" + userId +
                ", expiryDate=" + expiryDate +
                '}';
    }
}
