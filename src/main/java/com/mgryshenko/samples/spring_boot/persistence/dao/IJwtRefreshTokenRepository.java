package com.mgryshenko.samples.spring_boot.persistence.dao;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.IUserTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.JwtRefreshToken;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface IJwtRefreshTokenRepository extends IUserTokenRepository<JwtRefreshToken> {

    @Modifying
    @Query("update JwtRefreshToken j SET j.expiryDate = :date WHERE j.token = :token")
    void updateExpiryDateByToken(@Param("date") Date date, @Param("token") String token);

}
