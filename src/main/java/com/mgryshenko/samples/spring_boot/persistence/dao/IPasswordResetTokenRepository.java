package com.mgryshenko.samples.spring_boot.persistence.dao;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.IUserTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.PasswordResetToken;

public interface IPasswordResetTokenRepository extends IUserTokenRepository<PasswordResetToken> {
}
