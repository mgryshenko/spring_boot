package com.mgryshenko.samples.spring_boot.persistence.model;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "jwt_refresh_token")
public class JwtRefreshToken extends UserTokenEntity {
}
