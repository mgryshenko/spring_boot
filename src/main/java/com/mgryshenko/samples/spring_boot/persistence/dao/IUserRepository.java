package com.mgryshenko.samples.spring_boot.persistence.dao;

import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IUserRepository extends CrudRepository<UserAccount, Long> {
    <R> R findByEmail(String email, Class<R> reducedEntity);
    boolean existsUserAccountByEmail(String username);

    @Modifying
    @Query("update UserAccount u SET u.password = :password WHERE u.id = :id")
    void updatePasswordById(@Param("password") String password, @Param("id") Long id);

    @Modifying
    @Query("update UserAccount u SET u.enabled = :enabled WHERE u.id = :id")
    void updateEnabledById(@Param("enabled") boolean enabled, @Param("id") Long id);
}
