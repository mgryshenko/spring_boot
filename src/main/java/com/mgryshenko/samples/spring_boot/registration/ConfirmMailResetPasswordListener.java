package com.mgryshenko.samples.spring_boot.registration;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;
import com.mgryshenko.samples.spring_boot.service.password.IPasswordResetService;
import com.mgryshenko.samples.spring_boot.user.IUserIdEmailDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationListener;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "confirm_reset_password.mail", name = "enabled", havingValue = "true")
public class ConfirmMailResetPasswordListener implements ApplicationListener<OnResetPasswordCompleteEvent> {

    protected final IPasswordResetService service;
    protected final JavaMailSender mailSender;
    protected final ResourceBundleMessageSource msg;


    @Autowired
    public ConfirmMailResetPasswordListener(IPasswordResetService passwordResetService,
                                            JavaMailSender mailSender,
                                            ResourceBundleMessageSource msg) {
        this.service = passwordResetService;
        this.mailSender = mailSender;
        this.msg = msg;
    }

    @Override
    public void onApplicationEvent(OnResetPasswordCompleteEvent event) {
        sendResetPasswordMail(event);
    }

    private void sendResetPasswordMail(OnResetPasswordCompleteEvent e) {
        IUserIdEmailDetails userIdEmail = e.getUser();
        UserTokenEntity tokenEntity = service.tokens().create(userIdEmail);

        String address = userIdEmail.getEmail();
        String subject = msg.getMessage("mail.reset_password.title", null, e.getLocale());
        String confirmationUrl = e.getCompleteUri() + "/?token=" + tokenEntity.getToken();
        String content = String.format(msg.getMessage("mail.reset_password.body", null, e.getLocale()), confirmationUrl);

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(address);
        email.setSubject(subject);
        email.setText(createMessageBody(content, e.getLocale()));

        log.info("reset password: " + confirmationUrl);
        mailSender.send(email);
    }

    private String createMessageBody(String content, Locale locale) {
        return content + msg.getMessage("mail.footer", null, locale);
    }
}
