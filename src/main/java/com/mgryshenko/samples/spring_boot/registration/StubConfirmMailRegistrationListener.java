package com.mgryshenko.samples.spring_boot.registration;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;
import com.mgryshenko.samples.spring_boot.service.register.IRegisterUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "confirm_registration.mail", name = "enabled", havingValue = "false")
public class StubConfirmMailRegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    protected final IRegisterUserService service;

    @Autowired
    public StubConfirmMailRegistrationListener(IRegisterUserService service) {
        this.service = service;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent e) {
        UserTokenEntity tokenEntity = service.tokens().create(e.getUser());
        String confirmationUrl = e.getCompleteUri() + "/?token=" + tokenEntity.getToken();
        log.debug("confirm registration token: " + confirmationUrl);
    }
}
