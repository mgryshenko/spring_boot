package com.mgryshenko.samples.spring_boot.registration;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;
import com.mgryshenko.samples.spring_boot.service.password.IPasswordResetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "confirm_reset_password.mail", name = "enabled", havingValue = "false")
public class StubConfirmMailResetPasswordListener implements ApplicationListener<OnResetPasswordCompleteEvent> {

    protected final IPasswordResetService service;

    @Autowired
    public StubConfirmMailResetPasswordListener(IPasswordResetService service) {
        this.service = service;
    }

    @Override
    public void onApplicationEvent(OnResetPasswordCompleteEvent e) {
        UserTokenEntity tokenEntity = service.tokens().create(e.getUser());
        String confirmationUrl = e.getCompleteUri() + "/?token=" + tokenEntity.getToken();
        log.debug("confirm password reset: " + confirmationUrl);
    }
}
