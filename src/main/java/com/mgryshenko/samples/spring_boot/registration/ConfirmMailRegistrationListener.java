package com.mgryshenko.samples.spring_boot.registration;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;
import com.mgryshenko.samples.spring_boot.service.register.IRegisterUserService;
import com.mgryshenko.samples.spring_boot.user.IUserIdEmailDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationListener;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "confirm_registration.mail", name = "enabled", havingValue = "true")
public class ConfirmMailRegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    protected final IRegisterUserService service;
    protected final JavaMailSender mailSender;
    protected final ResourceBundleMessageSource msg;

    @Autowired
    public ConfirmMailRegistrationListener(IRegisterUserService registerUserService,
                                           JavaMailSender mailSender,
                                           ResourceBundleMessageSource msg) {
        this.service = registerUserService;
        this.mailSender = mailSender;
        this.msg = msg;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent e) {
        IUserIdEmailDetails userIdEmail = e.getUser();
        UserTokenEntity token = service.tokens().create(userIdEmail);

        String address = userIdEmail.getEmail();
        String subject = msg.getMessage("mail.register.title", null, e.getLocale());
        String confirmationUrl = e.getCompleteUri() + "/?token=" + token.getToken();
        String content = String.format(msg.getMessage("mail.register.body", null, e.getLocale()), confirmationUrl);

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(address);
        email.setSubject(subject);
        email.setText(createMessageBody(content, e.getLocale()));

        log.info("confirm registration: " + confirmationUrl);
        mailSender.send(email);
    }

    private String createMessageBody(String content, Locale locale) {
        return content + msg.getMessage("mail.footer", null, locale);
    }
}
