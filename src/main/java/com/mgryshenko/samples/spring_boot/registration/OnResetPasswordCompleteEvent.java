package com.mgryshenko.samples.spring_boot.registration;

import com.mgryshenko.samples.spring_boot.user.IUserIdEmailDetails;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

@Getter
@Setter
public class OnResetPasswordCompleteEvent extends ApplicationEvent {

    private final String completeUri;
    private final Locale locale;
    private final IUserIdEmailDetails user;

    public OnResetPasswordCompleteEvent(IUserIdEmailDetails user, String completeUri, Locale locale) {
        super(user);
        this.completeUri = completeUri;
        this.locale = locale;
        this.user = user;
    }
}
