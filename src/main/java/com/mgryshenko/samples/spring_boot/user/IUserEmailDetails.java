package com.mgryshenko.samples.spring_boot.user;

public interface IUserEmailDetails {
    String getEmail();
}
