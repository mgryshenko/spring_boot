package com.mgryshenko.samples.spring_boot.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Getter
@FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
public class UserIdEmailDetails implements IUserIdEmailDetails {
    Long id;
    String email;
}
