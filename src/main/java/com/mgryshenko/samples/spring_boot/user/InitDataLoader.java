package com.mgryshenko.samples.spring_boot.user;

import com.mgryshenko.samples.spring_boot.config.SecretsConfig;
import com.mgryshenko.samples.spring_boot.persistence.model.Authority;
import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.security.oauth.model.AuthProvider;
import com.mgryshenko.samples.spring_boot.service.Authorities;
import com.mgryshenko.samples.spring_boot.service.register.RegisterUserService;
import com.mgryshenko.samples.spring_boot.service.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Map;

import static com.mgryshenko.samples.spring_boot.service.Authorities.*;

@Component
public class InitDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean isInitialized;

    private final SecretsConfig secretsConfig;
    private final UserAccountBuilder builder;
    private final RegisterUserService registerUserService;
    private final Authorities authorities;
    private final Roles roles;

    @Autowired
    public InitDataLoader(SecretsConfig secretsConfig,
                          UserAccountBuilder builder,
                          RegisterUserService registerUserService,
                          Authorities authorities,
                          Roles roles) {
        this.secretsConfig = secretsConfig;
        this.builder = builder;
        this.registerUserService = registerUserService;
        this.authorities = authorities;
        this.roles = roles;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (isInitialized) {
            return;
        }

        Map<String, Authority> auth = authorities.initAuthorities();
        roles.initRole(Roles.USER, new ArrayList<>(){{
            add(auth.get(READ_USER));
            add(auth.get(WRITE_USER));
        }});
        roles.initRole(Roles.ADMIN, new ArrayList<>(){{
            add(auth.get(READ_USER));
            add(auth.get(WRITE_USER));
            add(auth.get(DELETE_USER));
        }});

        initAdminUser();

        isInitialized = true;
    }

    private void initAdminUser() {
        String email = secretsConfig.getAdminEmail();
        if (StringUtils.isEmpty(secretsConfig.getAdminEmail())) {
            return;
        }
        if (registerUserService.userExists(secretsConfig.getAdminEmail())) {
            return;
        }

        UserAccount admin = new UserAccount();
        admin.setName(secretsConfig.getAdminName());
        admin.setEmail(email);
        admin.setPassword(builder.passwordEncoder().encode(secretsConfig.getAdminPassword()));
        admin.setRoles(roles.withRole(Roles.ADMIN));
        admin.setProvider(AuthProvider.local);
        admin.setEnabled(true);

        registerUserService.registerUserAccount(admin);
    }
}
