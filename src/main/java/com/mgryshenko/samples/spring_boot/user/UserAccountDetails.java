package com.mgryshenko.samples.spring_boot.user;

import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.security.oauth.model.AuthProvider;
import com.mgryshenko.samples.spring_boot.security.user.IOAuth2UserDetails;
import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;
import com.mgryshenko.samples.spring_boot.service.Authorities;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@AllArgsConstructor
@Getter
public class UserAccountDetails implements IOAuth2UserDetails, IUserEmailDetails, IUserIdDetails, UserDetails {

    private final UserAccount user;
    private final Collection<? extends GrantedAuthority> authorities;

    public UserAccountDetails(UserAccount user) {
        this.user = user;
        authorities = Authorities.fromRoles(user.getRoles());
    }

    @Override
    public Long getId() {
        return user.getId();
    }

    //region UserDetails
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }
    //endregion UserDetails

    //region IOAuth2User
    @Override
    public String getName() {
        return user.getName();
    }

    @Override
    public String getEmail() {
        return user.getEmail();
    }

    @Override
    public String getImageUrl() {
        return user.getImageUrl();
    }

    @Override
    public String getProviderId() {
        return user.getProviderId();
    }

    @Override
    public AuthProvider getProvider() {
        return user.getProvider();
    }
    //endregion IOAuth2User
}
