package com.mgryshenko.samples.spring_boot.user;

import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;

public interface IUserIdEmailDetails extends IUserEmailDetails, IUserIdDetails {
}
