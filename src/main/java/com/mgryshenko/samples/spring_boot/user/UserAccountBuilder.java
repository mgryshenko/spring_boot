package com.mgryshenko.samples.spring_boot.user;

import com.mgryshenko.samples.spring_boot.security.oauth.model.AuthProvider;
import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.service.Roles;
import com.mgryshenko.samples.spring_boot.web.dto.UserAccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserAccountBuilder {
    
    protected final PasswordEncoder passwordEncoder;
    protected final Roles roles;

    @Autowired
    public UserAccountBuilder(PasswordEncoder passwordEncoder, Roles roles) {
        this.passwordEncoder = passwordEncoder;
        this.roles = roles;
    }

    public UserAccount toModel(UserAccountDto dto) {
        UserAccount model = new UserAccount();

        model.setName(dto.getName());
        model.setEmail(dto.getEmail());
        model.setPassword(encode(dto.getPassword()));
        model.setRoles(roles.withRole(Roles.USER));
        model.setProvider(AuthProvider.local);
        model.setEnabled(false);

        return model;
    }

    public UserAccountDto toDto(UserAccount model) {
        UserAccountDto dto = new UserAccountDto();

        dto.setName(model.getName());
        dto.setEmail(model.getEmail());
        addDebugInfo(model, dto);
        removeSensitiveInfo(dto);

        return dto;
    }

    public String encode(String password) {
        return passwordEncoder.encode(password);
    }

    public PasswordEncoder passwordEncoder() {
        return passwordEncoder;
    }

    public void addDebugInfo(UserAccount src, UserAccountDto dst) {
        dst.setEnabled(src.isEnabled());
        dst.setAccountId(src.getId());
        dst.setRoles(src.getRoles());
    }

    public void removeSensitiveInfo(UserAccountDto dto) {
        dto.setPassword(null);
    }
}
