package com.mgryshenko.samples.spring_boot.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegisterUserAccountDto extends UserAccountDto {

    @NotNull(message = "Complete URI must not be null")
    @NotEmpty(message = "Complete URI must not be empty")
    private String completeUri;
}
