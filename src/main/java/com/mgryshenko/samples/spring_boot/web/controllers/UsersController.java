package com.mgryshenko.samples.spring_boot.web.controllers;

import com.mgryshenko.samples.spring_boot.persistence.dao.IUserRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.service.IUserAccountService;
import com.mgryshenko.samples.spring_boot.user.UserAccountBuilder;
import com.mgryshenko.samples.spring_boot.web.dto.UserAccountDto;
import com.mgryshenko.samples.spring_boot.web.resources.UserAccountResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UsersController {

    protected final IUserRepository userRepository;
    protected final IUserAccountService userAccountService;
    protected final UserAccountBuilder builder;
    protected final UserAccountResourceAssembler resourceAssembler;

    @Autowired
    public UsersController(IUserRepository userRepository,
                           IUserAccountService userAccountService,
                           UserAccountBuilder builder,
                           UserAccountResourceAssembler resourceAssembler) {
        this.userRepository = userRepository;
        this.userAccountService = userAccountService;
        this.builder = builder;
        this.resourceAssembler = resourceAssembler;
    }

    @GetMapping(value = "/admin/users", produces = {MediaType.APPLICATION_JSON_VALUE})
    public CollectionModel<EntityModel<UserAccountDto>> allUsers() {
        List<EntityModel<UserAccountDto>> users = new ArrayList<>();
        userAccountService.findAll().forEach(userDetails ->
                users.add(resourceAssembler.toModel(userDetails.getUser()))
        );
        return new CollectionModel<>(users, resourceAssembler.linkToAllUsers().withSelfRel());
    }

    @GetMapping(value = "/admin/users/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public EntityModel<UserAccountDto> oneUser(@PathVariable Long id) {
        UserAccount user = userAccountService.findById(id).getUser();
        return resourceAssembler.toModel(user);
    }

    @DeleteMapping(value = "/admin/users/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public EntityModel<UserAccountDto> deleteUser(@PathVariable Long id) {
        UserAccount user = userAccountService.findById(id).getUser();
        userAccountService.deleteUser(user.getId());
        return new EntityModel<>(builder.toDto(user), resourceAssembler.linkToAllUsers());
    }
}
