package com.mgryshenko.samples.spring_boot.web.controllers;

import com.mgryshenko.samples.spring_boot.security.user.error.ResponseSendErrorException;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
public class ServletFilterErrorController extends AbstractErrorController {

    protected final ErrorProperties errorProperties;
    protected final ErrorAttributes errorAttributes;

    @Autowired
    public ServletFilterErrorController(ErrorAttributes errorAttributes, ServerProperties serverProperties) {
        super(errorAttributes);
        this.errorAttributes = errorAttributes;
        errorProperties = serverProperties.getError();
    }

    /**
     * Redirects error thrown withing {@link FilterChainProxy} to {@link ResponseEntityExceptionHandler}
     */
    @RequestMapping(path = "${server.error.path:${error.path:/error}}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public void error(HttpServletRequest request) throws ResponseSendErrorException {
        Map<String, Object> attributes = getErrorAttributes(request);
        log.debug("[Error][ErrorController] /error exception: " + attributes.get("path"));
        throw new ResponseSendErrorException(attributes, (Throwable) attributes.get("cause"));
    }

    /**
     * @return map with at least next entries:
     * - timestamp -> Thu Aug 01 13:28:03 EEST 2019
     * - status -> 500
     * - error -> Internal Server Error
     * - cause -> {@link Throwable}
     * - message -> Internal Server Error
     * - path -> /oauth2/authorize/facebooks
     */
    protected Map<String, Object> getErrorAttributes(HttpServletRequest request) {
        WebRequest webRequest = new ServletWebRequest(request);

        Map<String, Object> attributes =  errorAttributes.getErrorAttributes(webRequest, isIncludeStackTrace(request));
        attributes.put("cause", errorAttributes.getError(webRequest));

        return attributes;
    }

    protected boolean isIncludeStackTrace(HttpServletRequest request) {
        ErrorProperties.IncludeStacktrace include = errorProperties.getIncludeStacktrace();
        if (include == ErrorProperties.IncludeStacktrace.ALWAYS) {
            return true;
        }
        if (include == ErrorProperties.IncludeStacktrace.ON_TRACE_PARAM) {
            return getTraceParameter(request);
        }
        return false;
    }

    @Override
    public String getErrorPath() {
        return errorProperties.getPath();
    }
}
