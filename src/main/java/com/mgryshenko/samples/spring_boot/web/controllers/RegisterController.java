package com.mgryshenko.samples.spring_boot.web.controllers;

import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.registration.OnRegistrationCompleteEvent;
import com.mgryshenko.samples.spring_boot.service.AuthorizedRedirectUriService;
import com.mgryshenko.samples.spring_boot.service.register.IRegisterUserService;
import com.mgryshenko.samples.spring_boot.user.IUserIdEmailDetails;
import com.mgryshenko.samples.spring_boot.user.UserAccountBuilder;
import com.mgryshenko.samples.spring_boot.user.UserIdEmailDetails;
import com.mgryshenko.samples.spring_boot.web.dto.RegisterUserAccountDto;
import com.mgryshenko.samples.spring_boot.web.dto.UserAccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;

@RestController
public class RegisterController {

    protected final UserAccountBuilder builder;
    protected final IRegisterUserService service;
    protected final AuthorizedRedirectUriService redirectService;
    protected final ApplicationEventPublisher eventPublisher;

    @Autowired
    public RegisterController(IRegisterUserService service,
                              UserAccountBuilder builder,
                              AuthorizedRedirectUriService redirectService,
                              ApplicationEventPublisher eventPublisher) {
        this.service = service;
        this.builder = builder;
        this.redirectService = redirectService;
        this.eventPublisher = eventPublisher;
    }

    @PostMapping(value = "/auth/register", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public UserAccountDto register(@RequestBody @Valid RegisterUserAccountDto userDto, WebRequest request) {
        redirectService.authorizeUri(userDto.getCompleteUri());

        UserAccount userModel = service.registerUserAccount(builder.toModel(userDto));
        IUserIdEmailDetails userDetails = new UserIdEmailDetails(userModel.getId(), userModel.getEmail());
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(userDetails, userDto.getCompleteUri(), request.getLocale()));

        builder.addDebugInfo(userModel, userDto);
        builder.removeSensitiveInfo(userDto);
        return userDto;
    }

    @GetMapping(value = "/auth/register", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void confirmRegistration(@RequestParam("token") String token) {
        service.completeRegistration(token);
    }
}
