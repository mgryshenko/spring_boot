package com.mgryshenko.samples.spring_boot.web.dto;

import com.mgryshenko.samples.spring_boot.validation.EmailValid;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResetPasswordDto {

    @NotNull(message = "Email must not be null")
    @NotEmpty(message = "Email must not be empty")
    @EmailValid
    private String email;

    @NotNull(message = "Redirect URL must not be null")
    @NotEmpty(message = "Redirect URL must not be empty")
    private String completeUri;
}
