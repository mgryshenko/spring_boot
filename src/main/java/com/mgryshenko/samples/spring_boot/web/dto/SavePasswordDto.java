package com.mgryshenko.samples.spring_boot.web.dto;

import com.mgryshenko.samples.spring_boot.validation.IPasswordMatching;
import com.mgryshenko.samples.spring_boot.validation.PasswordStrong;
import com.mgryshenko.samples.spring_boot.validation.PasswordsMatched;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@PasswordsMatched
public class SavePasswordDto implements IPasswordMatching {

    @NotNull(message = "Reset token must not be null")
    @NotEmpty(message = "Reset token must not be empty")
    private String token;

    @NotNull(message = "Password must not be null")
    @NotEmpty(message = "Password must not be empty")
    @PasswordStrong
    private String password;

    @NotNull(message = "Matching password must not be null")
    @NotEmpty(message = "Matching password must not be empty")
    private String matchingPassword;
}
