package com.mgryshenko.samples.spring_boot.web.controllers;

import com.mgryshenko.samples.spring_boot.registration.OnResetPasswordCompleteEvent;
import com.mgryshenko.samples.spring_boot.security.user.error.InvalidResetPasswordTokenException;
import com.mgryshenko.samples.spring_boot.service.AuthorizedRedirectUriService;
import com.mgryshenko.samples.spring_boot.service.IUserAccountService;
import com.mgryshenko.samples.spring_boot.service.password.IPasswordResetService;
import com.mgryshenko.samples.spring_boot.service.token.error.InvalidTokenException;
import com.mgryshenko.samples.spring_boot.user.IUserIdEmailDetails;
import com.mgryshenko.samples.spring_boot.user.UserAccountBuilder;
import com.mgryshenko.samples.spring_boot.user.UserIdEmailDetails;
import com.mgryshenko.samples.spring_boot.web.dto.ChangePasswordDto;
import com.mgryshenko.samples.spring_boot.web.dto.ResetPasswordDto;
import com.mgryshenko.samples.spring_boot.web.dto.SavePasswordDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;

@Slf4j
@RestController
public class PasswordController {

    protected final IPasswordResetService passwordResetService;
    protected final ApplicationEventPublisher eventPublisher;
    protected final AuthorizedRedirectUriService redirectService;
    protected final IUserAccountService userService;
    protected final UserAccountBuilder builder;

    public PasswordController(IPasswordResetService passwordResetService,
                              ApplicationEventPublisher eventPublisher,
                              AuthorizedRedirectUriService redirectService,
                              IUserAccountService userService,
                              UserAccountBuilder builder) {
        this.passwordResetService = passwordResetService;
        this.eventPublisher = eventPublisher;
        this.redirectService = redirectService;
        this.userService = userService;
        this.builder = builder;
    }


    @PostMapping(value = "/auth/password/reset", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void resetPassword(@RequestBody @Valid ResetPasswordDto dto, WebRequest request) {
        redirectService.authorizeUri(dto.getCompleteUri());
        IUserIdEmailDetails user = userService.findByEmail(dto.getEmail(), UserIdEmailDetails.class);
        eventPublisher.publishEvent(new OnResetPasswordCompleteEvent(user, dto.getCompleteUri(), request.getLocale()));
    }

    @PostMapping(value = "/auth/password/save", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void savePassword(@RequestBody @Valid SavePasswordDto dto) {
        try {
            passwordResetService.resetPassword(dto.getToken(), dto.getPassword());
        } catch (InvalidTokenException e) {
            throw new InvalidResetPasswordTokenException();
        }
    }

    @PostMapping(value = "/auth/password/change", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changePassword(@RequestBody @Valid ChangePasswordDto dto) {
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        passwordResetService.changePassword(email, dto);
    }
}
