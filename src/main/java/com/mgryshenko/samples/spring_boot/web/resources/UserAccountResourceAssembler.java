package com.mgryshenko.samples.spring_boot.web.resources;

import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.user.UserAccountBuilder;
import com.mgryshenko.samples.spring_boot.web.controllers.UsersController;
import com.mgryshenko.samples.spring_boot.web.dto.UserAccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Component
public class UserAccountResourceAssembler implements RepresentationModelAssembler<UserAccount, EntityModel<UserAccountDto>> {

    protected final UserAccountBuilder builder;

    @Autowired
    public UserAccountResourceAssembler(UserAccountBuilder builder) {
        this.builder = builder;
    }

    @Override
    public EntityModel<UserAccountDto> toModel(UserAccount user) {
        return new EntityModel<>(builder.toDto(user),
                linkTo(methodOn(UsersController.class).oneUser(user.getId())).withSelfRel().withType(GET.name()),
                linkTo(methodOn(UsersController.class).deleteUser(user.getId())).withRel("delete").withType(DELETE.name()),
                linkToAllUsers()
        );
    }

    public Link linkToAllUsers() {
        return linkTo(methodOn(UsersController.class).allUsers()).withRel("users").withType(GET.name());
    }
}
