package com.mgryshenko.samples.spring_boot.web.controllers;

import com.mgryshenko.samples.spring_boot.persistence.dao.IUserRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;
import com.mgryshenko.samples.spring_boot.service.IUserAccountService;
import com.mgryshenko.samples.spring_boot.user.UserAccountDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserMeController {

    protected final IUserRepository userRepository;
    protected final IUserAccountService userAccountService;

    @Autowired
    public UserMeController(IUserRepository userRepository,
                            IUserAccountService userAccountService) {
        this.userRepository = userRepository;
        this.userAccountService = userAccountService;
    }

    @GetMapping(value = "/user/me", produces = {MediaType.APPLICATION_JSON_VALUE})
    public UserAccount getUserMe(Authentication authentication) {
        String email = authentication.getName();
        UserAccountDetails userAccountDetails = userAccountService.findByEmail(email);
        return userAccountDetails.getUser();
    }

    @DeleteMapping(value = "/user/me", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserMe(Authentication authentication) {
        String email = authentication.getName();
        IUserIdDetails user = userAccountService.findByEmail(email, IUserIdDetails.class);
        userAccountService.deleteUser((Long) user.getId());
    }
}
