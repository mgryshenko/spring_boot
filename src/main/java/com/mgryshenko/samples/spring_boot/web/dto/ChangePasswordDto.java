package com.mgryshenko.samples.spring_boot.web.dto;

import com.mgryshenko.samples.spring_boot.validation.IPasswordMatching;
import com.mgryshenko.samples.spring_boot.validation.PasswordStrong;
import com.mgryshenko.samples.spring_boot.validation.PasswordsMatched;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@PasswordsMatched(shouldMatch = false)
public class ChangePasswordDto implements IPasswordMatching {

    @NotNull(message = "Old password must not be null")
    @NotEmpty(message = "Old password must not be empty")
    private String oldPassword;

    @NotNull(message = "New password must not be null")
    @NotEmpty(message = "New password must not be empty")
    @PasswordStrong
    private String password;

    @Override
    public String getMatchingPassword() {
        return oldPassword;
    }
}
