package com.mgryshenko.samples.spring_boot.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mgryshenko.samples.spring_boot.persistence.model.Role;
import com.mgryshenko.samples.spring_boot.validation.EmailValid;
import com.mgryshenko.samples.spring_boot.validation.PasswordStrong;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAccountDto extends RepresentationModel {

    @NotNull(message = "Name must not be null")
    @NotEmpty(message = "Name must not be empty")
    private String name;

    @NotNull(message = "Email must not be null")
    @NotEmpty(message = "Email must not be empty")
    @EmailValid
    private String email;

    @NotNull(message = "Password must not be null")
    @NotEmpty(message = "Password must not be empty")
    @PasswordStrong
    private String password;

    //region DEBUG

    private boolean enabled;
    private long accountId;
    private Collection<Role> roles;

    //endregion DEBUG

}
