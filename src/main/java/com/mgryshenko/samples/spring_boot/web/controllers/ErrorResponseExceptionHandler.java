package com.mgryshenko.samples.spring_boot.web.controllers;

import com.mgryshenko.samples.spring_boot.security.error.handler.IExceptionHandlerResolver;
import com.mgryshenko.samples.spring_boot.security.error.model.ErrorData;
import com.mgryshenko.samples.spring_boot.security.error.model.ErrorDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ErrorResponseExceptionHandler extends ResponseEntityExceptionHandler {

    protected final ErrorDataBuilder builder = new ErrorDataBuilder();
    protected final ResourceBundleMessageSource msg;
    protected final IExceptionHandlerResolver resolver;

    @Autowired
    public ErrorResponseExceptionHandler(ResourceBundleMessageSource msg,
                                         IExceptionHandlerResolver exceptionHandlerResolver) {
        this.msg = msg;
        resolver = exceptionHandlerResolver;
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorData data = createErrorData(request, "error.internal_server_error", ex, status);
        return new ResponseEntity<>(data, headers, data.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorData data = createErrorData(request, "error.validation_failed", ex, BAD_REQUEST);
        data.addValidationFieldErrors(ex.getBindingResult().getFieldErrors());
        data.addValidationGlobalErrors(ex.getBindingResult().getGlobalErrors());
        return new ResponseEntity<>(data, data.getStatus());
    }

    @ExceptionHandler(Exception.class)
    public void runtimeException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        resolver.handle(ex, request, response);
    }

    public ErrorData createErrorData(WebRequest request, String msgKey, Exception ex, HttpStatus status) {
        ErrorData data = builder.dataWithMessage(status, null, ex, msg.getMessage(msgKey, null, request.getLocale()));
        data.setPath(request.getContextPath());
        return data;
    }
}
