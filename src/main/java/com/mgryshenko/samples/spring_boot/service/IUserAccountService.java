package com.mgryshenko.samples.spring_boot.service;

import com.mgryshenko.samples.spring_boot.user.UserAccountDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface IUserAccountService {
    <R> R findByEmail(String email, Class<R> reducedEntity) throws UsernameNotFoundException;
    UserAccountDetails findByEmail(String email) throws UsernameNotFoundException;
    UserAccountDetails findById(Long id) throws UsernameNotFoundException;
    Iterable<UserAccountDetails> findAll();
    void deleteUser(Long id);
}
