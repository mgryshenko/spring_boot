package com.mgryshenko.samples.spring_boot.service.token;

import java.util.Date;

public interface IExpiryDateTokenDetails {
    Date getExpiryDate();
}
