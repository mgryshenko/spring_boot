package com.mgryshenko.samples.spring_boot.service.register;

import com.mgryshenko.samples.spring_boot.config.TaskConfig;
import com.mgryshenko.samples.spring_boot.persistence.dao.IUserRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.persistence.model.VerificationToken;
import com.mgryshenko.samples.spring_boot.security.user.error.UserEmailExistsException;
import com.mgryshenko.samples.spring_boot.service.IUserAccountService;
import com.mgryshenko.samples.spring_boot.service.token.IUserTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service("registerUserService")
public class RegisterUserService implements IRegisterUserService {

    protected final TaskConfig taskConfig;
    protected final IUserRepository userRepository;
    protected final IUserAccountService userService;
    protected final IUserTokenService<VerificationToken> verificationTokenService;

    @Autowired
    public RegisterUserService(TaskConfig taskConfig,
                               IUserRepository userRepository,
                               IUserAccountService userService,
                               VerificationTokenService verificationTokenService) {
        this.taskConfig = taskConfig;
        this.userRepository = userRepository;
        this.userService = userService;
        this.verificationTokenService = verificationTokenService;
    }

    @Transactional
    @Override
    public UserAccount registerUserAccount(UserAccount user) throws UserEmailExistsException {
        if (userExists(user.getEmail())) {
            throw new UserEmailExistsException(user.getEmail());
        }

        return userRepository.save(user);
    }

    @Transactional
    @Override
    public void completeRegistration(String token) {
        VerificationToken tokenEntity = verificationTokenService.findValidByToken(token, VerificationToken.class);
        userRepository.updateEnabledById(true, tokenEntity.getUserId());
        log.debug("enabled user with ID: " + tokenEntity.getUserId());
        verificationTokenService.delete(tokenEntity);
    }

    @Override
    public boolean userExists(String email) {
        return userRepository.existsUserAccountByEmail(email);
    }

    @Override
    public IUserTokenService<VerificationToken> tokens() {
        return verificationTokenService;
    }
}
