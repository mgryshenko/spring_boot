package com.mgryshenko.samples.spring_boot.service.token.error;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@Getter
@FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
public class TokenExpiredException extends InvalidTokenException {
    String token;
    Date expiryDate;

    public TokenExpiredException(String token, Date expiryDate) {
        super("Token expired and can't be used");
        this.token = token;
        this.expiryDate = expiryDate;
    }
}
