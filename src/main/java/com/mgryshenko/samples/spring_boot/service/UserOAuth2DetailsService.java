package com.mgryshenko.samples.spring_boot.service;

import com.mgryshenko.samples.spring_boot.persistence.dao.IUserRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtRefreshDetails;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtRefreshDetailsService;
import com.mgryshenko.samples.spring_boot.security.oauth.error.NoEmailInfoOAuth2Exception;
import com.mgryshenko.samples.spring_boot.security.oauth.error.UnexpectedProviderOAuth2Exception;
import com.mgryshenko.samples.spring_boot.security.oauth.error.UserAccountNotFoundOAuth2Exception;
import com.mgryshenko.samples.spring_boot.security.oauth.error.UserEmailExistsOAuth2Exception;
import com.mgryshenko.samples.spring_boot.security.oauth.model.ISuccessRedirectToken;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IUserOAuth2DetailsService;
import com.mgryshenko.samples.spring_boot.security.user.IOAuth2UserDetails;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserOAuth2DetailsService implements IUserOAuth2DetailsService {

    protected final IUserRepository userRepository;
    protected final IJwtRefreshDetailsService jwtRefreshService;
    protected final UserAccountDetailsService userAccountDetailsService;
    protected final Roles roles;

    public UserOAuth2DetailsService(IUserRepository userRepository,
                                    IJwtRefreshDetailsService jwtRefreshService,
                                    UserAccountDetailsService userAccountDetailsService,
                                    Roles roles) {
        this.userRepository = userRepository;
        this.jwtRefreshService = jwtRefreshService;
        this.userAccountDetailsService = userAccountDetailsService;
        this.roles = roles;
    }

    @Override
    public ISuccessRedirectToken login(IOAuth2UserDetails info) {
        UserAccount existentUser = loadUser(info);

        if (existentUser == null) {
            throw new UserAccountNotFoundOAuth2Exception(info.getEmail());
        }

        if (existentUser.getProvider() != info.getProvider()) {
            if (!existentUser.getProvider().isLocal()) {
                throw new UnexpectedProviderOAuth2Exception(info.getProvider().name(), existentUser.getProvider().name());
            }
            switchToSocialProvider(existentUser, info);
        }

        existentUser.setName(info.getName());
        existentUser.setImageUrl(info.getImageUrl());

        UserAccount refreshedUser = userRepository.save(existentUser);
        return createSuccessToken(refreshedUser);
    }

    @Override
    public ISuccessRedirectToken register(IOAuth2UserDetails info) {
        UserAccount existentUser = loadUser(info);
        if (existentUser != null) {
            throw new UserEmailExistsOAuth2Exception(info.getEmail(), existentUser.getProvider().name());
        }

        UserAccount registeredUser = userRepository.save(toUserAccount(info));
        return createSuccessToken(registeredUser);
    }

    private ISuccessRedirectToken createSuccessToken(UserAccount user) {
        String loginToken = createRefreshToken(user);
        return () -> loginToken;
    }

    private String createRefreshToken(UserAccount user) {
        IJwtRefreshDetails refreshToken = jwtRefreshService.create(user::getId);
        return refreshToken.getToken();
    }

    private void switchToSocialProvider(UserAccount user, IOAuth2UserDetails info) {
        user.setProvider(info.getProvider());
        user.setProviderId(info.getProviderId());
        user.setPassword(null);
    }

    private UserAccount loadUser(IOAuth2UserDetails info) {
        String email = info.getEmail();
        if (StringUtils.isEmpty(email)) {
            throw new NoEmailInfoOAuth2Exception(info.getProvider().name());
        }
        return userAccountDetailsService.findByEmail(email, UserAccount.class);
    }

    private UserAccount toUserAccount(IOAuth2UserDetails info) {
        UserAccount model = new UserAccount();

        model.setName(info.getName());
        model.setEmail(info.getEmail());
        model.setImageUrl(info.getImageUrl());
        model.setRoles(roles.withRole(Roles.USER));
        model.setProvider(info.getProvider());
        model.setProviderId(info.getProviderId());
        model.setEnabled(true);

        return model;
    }
}
