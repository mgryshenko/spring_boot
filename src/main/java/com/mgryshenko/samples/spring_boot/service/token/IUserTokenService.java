package com.mgryshenko.samples.spring_boot.service.token;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;
import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;
import com.mgryshenko.samples.spring_boot.service.token.error.InvalidTokenException;

public interface IUserTokenService<T extends UserTokenEntity> {
    T create(IUserIdDetails userId);
    <R> R findByToken(String token, Class<R> type);
    <V extends IExpiryDateTokenDetails> V findValidByToken(String token, Class<V> type) throws InvalidTokenException;
    void delete(T token);
}
