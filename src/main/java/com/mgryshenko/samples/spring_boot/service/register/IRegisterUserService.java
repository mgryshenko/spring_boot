package com.mgryshenko.samples.spring_boot.service.register;

import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.persistence.model.VerificationToken;
import com.mgryshenko.samples.spring_boot.security.user.error.UserEmailExistsException;
import com.mgryshenko.samples.spring_boot.service.token.IUserTokenService;
import com.mgryshenko.samples.spring_boot.service.token.error.InvalidTokenException;

public interface IRegisterUserService {
    UserAccount registerUserAccount(UserAccount user) throws UserEmailExistsException;
    void completeRegistration(String token) throws InvalidTokenException;
    boolean userExists(String email);
    IUserTokenService<VerificationToken> tokens();
}
