package com.mgryshenko.samples.spring_boot.service;

import com.mgryshenko.samples.spring_boot.config.CorsConfig;
import com.mgryshenko.samples.spring_boot.security.cors.IAuthorizedOriginUriService;
import com.mgryshenko.samples.spring_boot.security.cors.MalformedUriException;
import com.mgryshenko.samples.spring_boot.security.cors.UntrustedOriginUriException;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorizedRedirectUriService implements IAuthorizedOriginUriService {

    protected final List<URI> authorizedUris;

    @Autowired
    public AuthorizedRedirectUriService(CorsConfig corsConfig) {
        authorizedUris = corsConfig.getAuthorizedUris().stream()
                .map(this::toUri)
                .collect(Collectors.toList());
    }

    @Override
    public URI authorizeUri(String uri) {
        URI redirectUri = toUri(uri);

        if (!isAuthorizedUri(redirectUri)) {
            throw new UntrustedOriginUriException(uri);
        }

        return redirectUri;
    }

    private boolean isAuthorizedUri(URI uri) throws MalformedUriException {
        return authorizedUris
                .stream()
                .anyMatch(authorizedUri -> schemeAndHostAndPortMatched(authorizedUri, uri));
    }

    private boolean schemeAndHostAndPortMatched(URI u1, URI u2) {
        return u1.getScheme().equalsIgnoreCase(u2.getScheme())
                && u1.getHost().equalsIgnoreCase(u2.getHost())
                && (u1.getPort() == u2.getPort());
    }

    private URI toUri(String path) throws MalformedUriException {
        try {
            URI uri =  URI.create(path);
            if (uri.getHost() != null) {
                return uri;
            }
        } catch (IllegalArgumentException ignored) {}
        throw new MalformedUriException(path);
    }
}
