package com.mgryshenko.samples.spring_boot.service;

import com.mgryshenko.samples.spring_boot.persistence.dao.IRoleRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.Authority;
import com.mgryshenko.samples.spring_boot.persistence.model.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Slf4j
@Service
public class Roles {

    public static final String USER = "USER";
    public static final String ADMIN = "ADMIN";

    protected final IRoleRepository roleRepository;

    @Autowired
    public Roles(IRoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Transactional
    public Role initRole(String name, Collection<Authority> authorities) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role();
            role.setName(name);
            role.setAuthorities(authorities);
            roleRepository.save(role);
        }
        return role;
    }

    public Collection<Role> withRole(String name) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            log.warn("Role not found: " + name);
            return Collections.emptyList();
        }
        return new ArrayList<>(){{
            add(role);
        }};
    }
}
