package com.mgryshenko.samples.spring_boot.service.password;

import com.mgryshenko.samples.spring_boot.config.TaskConfig;
import com.mgryshenko.samples.spring_boot.persistence.dao.IPasswordResetTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.PasswordResetToken;
import com.mgryshenko.samples.spring_boot.service.token.UuidUserTokenService;
import org.springframework.stereotype.Service;

@Service("passwordResetTokenService")
public class PasswordResetTokenService extends UuidUserTokenService<PasswordResetToken> {

    protected PasswordResetTokenService(IPasswordResetTokenRepository passwordResetTokenRepository,
                                        TaskConfig taskConfig) {
        super(passwordResetTokenRepository);
        setExpirationPeriodSec(taskConfig.getConfirmResetPasswordExpirationSec());
    }

    @Override
    protected PasswordResetToken createTokenEntity() {
        return new PasswordResetToken();
    }
}
