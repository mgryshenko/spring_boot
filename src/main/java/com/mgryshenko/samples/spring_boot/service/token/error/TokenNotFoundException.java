package com.mgryshenko.samples.spring_boot.service.token.error;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@Getter
@FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
public class TokenNotFoundException extends InvalidTokenException {

    String token;

    public TokenNotFoundException(String token) {
        super("Token not found");
        this.token = token;
    }
}
