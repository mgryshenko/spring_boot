package com.mgryshenko.samples.spring_boot.service.register;

import com.mgryshenko.samples.spring_boot.config.TaskConfig;
import com.mgryshenko.samples.spring_boot.persistence.dao.IVerificationTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.VerificationToken;
import com.mgryshenko.samples.spring_boot.service.token.UuidUserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("verificationTokenService")
public class VerificationTokenService extends UuidUserTokenService<VerificationToken> {
    @Autowired
    protected VerificationTokenService(IVerificationTokenRepository tokenRepository, TaskConfig taskConfig) {
        super(tokenRepository);
        setExpirationPeriodSec(taskConfig.getConfirmRegistrationExpirationSec());
    }

    @Override
    protected VerificationToken createTokenEntity() {
        return new VerificationToken();
    }
}
