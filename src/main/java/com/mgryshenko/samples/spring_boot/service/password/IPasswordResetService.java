package com.mgryshenko.samples.spring_boot.service.password;

import com.mgryshenko.samples.spring_boot.persistence.model.PasswordResetToken;
import com.mgryshenko.samples.spring_boot.security.user.error.InvalidOldPasswordException;
import com.mgryshenko.samples.spring_boot.service.token.IUserTokenService;
import com.mgryshenko.samples.spring_boot.service.token.error.InvalidTokenException;
import com.mgryshenko.samples.spring_boot.web.dto.ChangePasswordDto;

public interface IPasswordResetService {
    void changePassword(String email, ChangePasswordDto passwords) throws InvalidOldPasswordException;
    void resetPassword(String token, String rawPassword) throws InvalidTokenException;
    IUserTokenService<PasswordResetToken> tokens();
}
