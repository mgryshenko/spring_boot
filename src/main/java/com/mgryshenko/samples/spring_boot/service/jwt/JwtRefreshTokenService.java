package com.mgryshenko.samples.spring_boot.service.jwt;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.IUserTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.JwtRefreshToken;
import com.mgryshenko.samples.spring_boot.service.token.UuidUserTokenService;
import org.springframework.stereotype.Service;

@Service("jwtRefreshTokenService")
public class JwtRefreshTokenService extends UuidUserTokenService<JwtRefreshToken> {

    private static final long MIN_PER_WEEK = 60 * 24 * 7;

    protected JwtRefreshTokenService(IUserTokenRepository<JwtRefreshToken> tokenRepository) {
        super(tokenRepository);
        setExpirationPeriodSec(MIN_PER_WEEK);
    }

    @Override
    protected JwtRefreshToken createTokenEntity() {
        return new JwtRefreshToken();
    }
}
