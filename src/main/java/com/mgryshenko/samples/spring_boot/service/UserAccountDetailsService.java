package com.mgryshenko.samples.spring_boot.service;

import com.mgryshenko.samples.spring_boot.persistence.dao.IJwtRefreshTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.dao.IPasswordResetTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.dao.IUserRepository;
import com.mgryshenko.samples.spring_boot.persistence.dao.IVerificationTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.security.user.error.UserAccountNotFoundException;
import com.mgryshenko.samples.spring_boot.user.UserAccountDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service("userDetailsService")
public class UserAccountDetailsService implements UserDetailsService, IUserAccountService {

    protected final IUserRepository userRepository;
    protected final IJwtRefreshTokenRepository refreshRepository;
    protected final IVerificationTokenRepository verifyRepository;
    protected final IPasswordResetTokenRepository resetRepository;

    @Autowired
    public UserAccountDetailsService(IUserRepository userRepository,
                                     IJwtRefreshTokenRepository refreshRepository,
                                     IVerificationTokenRepository verifyRepository,
                                     IPasswordResetTokenRepository resetRepository) {
        this.userRepository = userRepository;
        this.refreshRepository = refreshRepository;
        this.verifyRepository = verifyRepository;
        this.resetRepository = resetRepository;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return findByEmail(username);
    }

    @Transactional
    @Override
    public void deleteUser(Long id) {
        UserAccount user = UserAccount.withId(id);

        refreshRepository.deleteAllByUser(user);
        verifyRepository.deleteAllByUser(user);
        resetRepository.deleteAllByUser(user);
        userRepository.delete(user);
    }

    @Override
    public <R> R findByEmail(String email, Class<R> reducedEntity) {
        R user = userRepository.findByEmail(email, reducedEntity);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        return user;
    }

    @Transactional
    @Override
    public UserAccountDetails findByEmail(String email) throws UsernameNotFoundException {
        UserAccount user = findByEmail(email, UserAccount.class);
        return new UserAccountDetails(user);
    }

    @Transactional
    @Override
    public UserAccountDetails findById(Long id) throws UsernameNotFoundException {
        UserAccount user = userRepository.findById(id).orElseThrow(() -> new UserAccountNotFoundException(id));
        return new UserAccountDetails(user);
    }

    //TODO Delete this terrible method!!!
    @Transactional
    @Override
    public Iterable<UserAccountDetails> findAll() {
        List<UserAccountDetails> all = new ArrayList<>();
        userRepository.findAll().forEach(userAccount -> all.add(new UserAccountDetails(userAccount)));
        return all;
    }
}
