package com.mgryshenko.samples.spring_boot.service;

import com.mgryshenko.samples.spring_boot.persistence.dao.IAuthorityRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.Authority;
import com.mgryshenko.samples.spring_boot.persistence.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Service
public class Authorities {

    public static final String ROLE_PREFIX = "ROLE_";
    public static final String READ_USER = "READ_USER";
    public static final String WRITE_USER = "WRITE_USER";
    public static final String DELETE_USER = "DELETE_USER";

    protected final IAuthorityRepository authorityRepository;

    @Autowired
    public Authorities(IAuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    @Transactional
    public Map<String, Authority> initAuthorities() {
        Map<String, Authority> map = new HashMap<>();
        map.put(READ_USER, initAuthority(READ_USER));
        map.put(WRITE_USER, initAuthority(WRITE_USER));
        map.put(DELETE_USER, initAuthority(DELETE_USER));
        return map;
    }

    @Transactional
    public Authority initAuthority(String name) {
        Authority auth = authorityRepository.findByName(name);
        if (auth == null) {
            auth = new Authority();
            auth.setName(name);
            auth = authorityRepository.save(auth);
        }
        return auth;
    }

    public static Collection<? extends GrantedAuthority> fromNames(Collection<String> authorities) {
        return authorities.stream()
                .map(Authorities::fromName)
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }

    public static Collection<? extends GrantedAuthority> fromRoles(Collection<Role> roles) {
        List<String> authorityNames = new ArrayList<>();
        roles.forEach(role -> {
            role.getAuthorities().forEach(authority -> authorityNames.add(authority.getName()));
            authorityNames.add(ROLE_PREFIX + role.getName());
        });
        return fromNames(authorityNames);
    }

    public static GrantedAuthority fromName(String authorityName) {
        return new SimpleGrantedAuthority(authorityName);
    }
}
