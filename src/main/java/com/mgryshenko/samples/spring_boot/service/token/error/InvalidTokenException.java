package com.mgryshenko.samples.spring_boot.service.token.error;

public abstract class InvalidTokenException extends RuntimeException {

    public InvalidTokenException(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidTokenException(String msg) {
        super(msg);
    }
}
