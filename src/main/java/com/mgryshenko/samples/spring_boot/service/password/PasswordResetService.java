package com.mgryshenko.samples.spring_boot.service.password;

import com.mgryshenko.samples.spring_boot.config.TaskConfig;
import com.mgryshenko.samples.spring_boot.persistence.dao.IUserRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.PasswordResetToken;
import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;
import com.mgryshenko.samples.spring_boot.security.user.error.InvalidOldPasswordException;
import com.mgryshenko.samples.spring_boot.service.IUserAccountService;
import com.mgryshenko.samples.spring_boot.service.token.IUserTokenService;
import com.mgryshenko.samples.spring_boot.user.UserAccountBuilder;
import com.mgryshenko.samples.spring_boot.web.dto.ChangePasswordDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service("resetPasswordService")
public class PasswordResetService implements IPasswordResetService {

    protected final TaskConfig taskConfig;
    protected final UserAccountBuilder builder;
    protected final IUserRepository userRepository;
    protected final IUserAccountService userAccountService;
    protected final IUserTokenService<PasswordResetToken> tokens;

    @Autowired
    public PasswordResetService(TaskConfig taskConfig,
                                UserAccountBuilder builder,
                                IUserRepository userRepository,
                                IUserAccountService userAccountService, PasswordResetTokenService passwordResetTokenService) {
        this.taskConfig = taskConfig;
        this.builder = builder;
        this.userRepository = userRepository;
        this.userAccountService = userAccountService;
        this.tokens = passwordResetTokenService;
    }

    @Transactional
    @Override
    public void changePassword(String email, ChangePasswordDto passwords) {
        UserIdAndPasswordDto user = userAccountService.findByEmail(email, UserIdAndPasswordDto.class);
        if (!builder.passwordEncoder().matches(passwords.getOldPassword(), user.getPassword())) {
            throw new InvalidOldPasswordException();
        }
        updatePassword(user.getId(), passwords.getPassword());
    }

    @Transactional
    @Override
    public void resetPassword(String token, String rawPassword) {
        PasswordResetToken tokenEntity = tokens.findValidByToken(token, PasswordResetToken.class);
        updatePassword(tokenEntity.getUserId(), rawPassword);
        tokens.delete(PasswordResetToken.withId(tokenEntity.getId()));
    }

    private void updatePassword(Long userId, String rawPassword) {
        log.debug("change password, user ID: " + userId);
        String encodedPassword = builder.encode(rawPassword);
        userRepository.updatePasswordById(encodedPassword, userId);
    }

    @Override
    public IUserTokenService<PasswordResetToken> tokens() {
        return tokens;
    }

    @AllArgsConstructor
    @Getter
    @FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
    public static class UserIdAndPasswordDto implements IUserIdDetails {
        Long id;
        String password;
    }
}
