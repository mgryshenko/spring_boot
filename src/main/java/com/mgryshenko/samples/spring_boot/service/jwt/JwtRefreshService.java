package com.mgryshenko.samples.spring_boot.service.jwt;

import com.mgryshenko.samples.spring_boot.persistence.dao.IJwtRefreshTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.dao.IUserRepository;
import com.mgryshenko.samples.spring_boot.persistence.model.JwtRefreshToken;
import com.mgryshenko.samples.spring_boot.security.jwt.error.JwtRefreshExpiredException;
import com.mgryshenko.samples.spring_boot.security.jwt.error.JwtRefreshNotFoundException;
import com.mgryshenko.samples.spring_boot.security.jwt.error.JwtRefreshValidationException;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtRefreshDetails;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtRefreshDetailsService;
import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;
import com.mgryshenko.samples.spring_boot.service.IUserAccountService;
import com.mgryshenko.samples.spring_boot.service.token.IExpiryDateTokenDetails;
import com.mgryshenko.samples.spring_boot.service.token.error.InvalidTokenException;
import com.mgryshenko.samples.spring_boot.service.token.error.TokenNotFoundException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Slf4j
@Service("jwtRefreshService")
public class JwtRefreshService implements IJwtRefreshDetailsService {

    protected final IJwtRefreshTokenRepository repository;
    protected final JwtRefreshTokenService tokens;
    protected final IUserRepository userRepository;
    protected final IUserAccountService userAccountService;

    @Autowired
    public JwtRefreshService(IJwtRefreshTokenRepository repository,
                             IUserRepository userRepository,
                             JwtRefreshTokenService jwtRefreshTokenService,
                             IUserAccountService userAccountService) {
        this.repository = repository;
        this.userRepository = userRepository;
        tokens = jwtRefreshTokenService;
        this.userAccountService = userAccountService;
    }

    @Override
    public IJwtRefreshDetails create(IUserIdDetails user) {
        JwtRefreshToken tokenEntity = tokens.create(user);
        return new JwtRefreshDetails(tokenEntity.getToken(), tokenEntity.getExpiryDate());
    }

    @Override
    public UserDetails findUser(String payload) throws JwtRefreshValidationException {
        Long userId = findUserIdByToken(payload);
        return userAccountService.findById(userId);
    }

    private Long findUserIdByToken(String payload) throws JwtRefreshValidationException {
        UserIdAndExpiryDateDto tokenEntity;
        try {
            tokenEntity = tokens.findValidByToken(payload, UserIdAndExpiryDateDto.class);
        } catch (TokenNotFoundException e) {
            throw new JwtRefreshNotFoundException();
        } catch (InvalidTokenException e) {
            throw new JwtRefreshExpiredException();
        }
        return tokenEntity.getUserId();
    }

    @Transactional
    @Override
    public IJwtRefreshDetails refresh(String payload) throws JwtRefreshValidationException {
        Date expiryDate = tokens.expiryDateFromNow();
        repository.updateExpiryDateByToken(expiryDate, payload);

        log.debug("updated refresh token: " + payload);
        return new JwtRefreshDetails(payload, expiryDate);
    }

    @AllArgsConstructor
    @Getter
    @FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
    public static class UserIdAndExpiryDateDto implements IExpiryDateTokenDetails {
        Long userId;
        Date expiryDate;
    }

    @AllArgsConstructor
    @Getter
    @FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
    private static class JwtRefreshDetails implements IJwtRefreshDetails {
        String token;
        Date expiryDate;
    }
}
