package com.mgryshenko.samples.spring_boot.service.token;

import com.mgryshenko.samples.spring_boot.persistence.dao.token.IUserTokenRepository;
import com.mgryshenko.samples.spring_boot.persistence.dao.token.UserTokenEntity;
import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;
import com.mgryshenko.samples.spring_boot.service.token.error.TokenExpiredException;
import com.mgryshenko.samples.spring_boot.service.token.error.TokenNotFoundException;
import lombok.extern.slf4j.Slf4j;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Slf4j
public abstract class UuidUserTokenService<T extends UserTokenEntity> implements IUserTokenService<T> {

    public static final long DEFAULT_EXPIRATION_MIN = 60 * 24;

    protected final IUserTokenRepository<T> tokenRepository;

    protected long expirationPeriodSec = DEFAULT_EXPIRATION_MIN;

    protected UuidUserTokenService(IUserTokenRepository<T> tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public void setExpirationPeriodSec(long expirationPeriodSec) {
        this.expirationPeriodSec = expirationPeriodSec;
    }

    @Override
    public T create(IUserIdDetails userId) {
        T entity = createTokenEntity();
        entity.setToken(generateToken());
        entity.setUser(UserAccount.withId((Long) userId.getId()));
        entity.setExpiryDate(expiryDateFromNow());

        log.debug("created token: " + entity);
        return tokenRepository.save(entity);
    }

    abstract protected T createTokenEntity();

    private String generateToken() {
        return UUID.randomUUID().toString();
    }

    @Override
    public <R> R findByToken(String token, Class<R> type) {
        return tokenRepository.findByToken(token, type);
    }

    @Override
    public <V extends IExpiryDateTokenDetails> V findValidByToken(String token, Class<V> type) {
        V entity = findByToken(token, type);
        if (entity == null) {
            throw new TokenNotFoundException(token);
        }

        Date expiryDate = entity.getExpiryDate();
        if ((expiryDate == null) || isExpired(expiryDate)) {
            throw new TokenExpiredException(token, expiryDate);
        }

        return entity;
    }

    public boolean isExpired(Date expirationDate) {
        Calendar calendar = Calendar.getInstance();
        long expiration = expirationDate.getTime() - calendar.getTime().getTime();
        return expiration <= 0;
    }

    @Override
    public void delete(T tokenEntity) {
        tokenRepository.delete(tokenEntity);
    }

    public Date expiryDateFromNow() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Timestamp(calendar.getTime().getTime()));
        calendar.add(Calendar.MINUTE, (int) expirationPeriodSec);
        return new Date(calendar.getTime().getTime());
    }
}
