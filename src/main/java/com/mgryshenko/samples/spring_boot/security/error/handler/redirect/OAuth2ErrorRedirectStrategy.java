package com.mgryshenko.samples.spring_boot.security.error.handler.redirect;

import com.mgryshenko.samples.spring_boot.security.error.model.IErrorData;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IAuthorizedOauth2UriService;
import com.mgryshenko.samples.spring_boot.security.user.error.ResponseSendErrorException;
import java.net.URI;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

public class OAuth2ErrorRedirectStrategy implements IErrorRedirectStrategy {

    private final IAuthorizedOauth2UriService authorizedUriService;
    private final String pathPrefix;

    public OAuth2ErrorRedirectStrategy(IAuthorizedOauth2UriService authorizedUriService,
                                       String pathPrefix) {
        this.authorizedUriService = authorizedUriService;
        this.pathPrefix = pathPrefix;
    }

    @Override
    public URI resolveRedirectUri(HttpServletRequest request, Throwable ex, IErrorData data) {
        if (!requireRedirect(ex, data)) {
            return null;
        }
        try {
            return authorizedUriService.authorizeRedirectUri(request);
        } catch (Exception ignored) {
            return null;
        }
    }

    public boolean requireRedirect(Throwable ex, IErrorData data) {
        String requestPath;
        if (ex instanceof ResponseSendErrorException) {
            requestPath = (String) ((ResponseSendErrorException) ex).getErrorAttributes().get("path");
        } else {
            requestPath = data.getPath();
        }

        return StringUtils.isNotBlank(requestPath) && requestPath.startsWith(pathPrefix);
    }
}
