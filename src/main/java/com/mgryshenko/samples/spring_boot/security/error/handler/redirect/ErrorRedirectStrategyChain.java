package com.mgryshenko.samples.spring_boot.security.error.handler.redirect;

import com.mgryshenko.samples.spring_boot.security.error.model.IErrorData;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class ErrorRedirectStrategyChain implements IErrorRedirectStrategy {

    private final List<IErrorRedirectStrategy> strategies = new LinkedList<>();

    public void add(IErrorRedirectStrategy strategy) {
        strategies.add(strategy);
    }

    @Override
    public URI resolveRedirectUri(HttpServletRequest request, Throwable ex, IErrorData data) {
        URI uri = null;
        for (IErrorRedirectStrategy s : strategies) {
            uri = s.resolveRedirectUri(request, ex, data);
            if (uri != null) {
                return uri;
            }
        }
        return uri;
    }
}
