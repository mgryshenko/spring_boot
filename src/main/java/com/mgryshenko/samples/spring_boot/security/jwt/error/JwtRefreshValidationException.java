package com.mgryshenko.samples.spring_boot.security.jwt.error;

public class JwtRefreshValidationException extends JwtAuthenticationException {
    public JwtRefreshValidationException() {
        this("Refresh JWT provided is not valid or expired");
    }

    public JwtRefreshValidationException(String msg) {
        super(msg);
    }

    @Override
    public String getMsgKey() {
        return "error.auth.failed_jwt_refresh";
    }
}
