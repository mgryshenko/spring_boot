package com.mgryshenko.samples.spring_boot.security.jwt.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class JwtRefreshDto {

    @NotNull(message = "Refresh token shouldn't be null")
    @NotEmpty(message = "Refresh token shouldn't be empty")
    private String refreshToken;
}
