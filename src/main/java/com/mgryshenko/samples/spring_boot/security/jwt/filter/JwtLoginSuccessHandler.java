package com.mgryshenko.samples.spring_boot.security.jwt.filter;

import com.mgryshenko.samples.spring_boot.security.format.HttpResponseFormatter;
import com.mgryshenko.samples.spring_boot.security.jwt.JwtConfig;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtAccessDetails;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtRefreshDetails;
import com.mgryshenko.samples.spring_boot.security.jwt.model.JwtDto;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtAccessDetailsService;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtRefreshDetailsService;
import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;
import java.io.IOException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JwtLoginSuccessHandler implements AuthenticationSuccessHandler {

    protected final HttpResponseFormatter formatter = new HttpResponseFormatter();

    protected final IJwtRefreshDetailsService jwtRefreshService;
    protected final IJwtAccessDetailsService jwtAccessService;
    protected final JwtConfig jwtConfig;

    public JwtLoginSuccessHandler(IJwtRefreshDetailsService jwtRefreshService,
                                  IJwtAccessDetailsService jwtAccessService,
                                  JwtConfig jwtConfig) {
        this.jwtRefreshService = jwtRefreshService;
        this.jwtAccessService = jwtAccessService;
        this.jwtConfig = jwtConfig;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) throws IOException, ServletException {
        // no further filter chaining required
        onAuthenticationSuccess(request, response, authentication);
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException {
        IJwtAccessDetails access = provideAccessToken(auth);
        IJwtRefreshDetails refresh = provideRefreshToken(auth);

        formatJwtData(response, access, refresh);
    }

    protected IJwtAccessDetails provideAccessToken(Authentication auth) {
        return jwtAccessService.create(auth.getName(), auth.getAuthorities());
    }

    protected IJwtRefreshDetails provideRefreshToken(Authentication auth) {
        IUserIdDetails userDetails = (IUserIdDetails) auth.getPrincipal();
        return jwtRefreshService.create(userDetails);
    }

    public void formatJwtData(HttpServletResponse response, IJwtAccessDetails access, IJwtRefreshDetails refresh) throws IOException {
        formatter.addStatus(response, HttpServletResponse.SC_CREATED);
        formatter.addHeader(response, jwtConfig.getHeader(), jwtConfig.getSchemePrefix() + access.getToken());
        formatter.addHeader(response, jwtConfig.getRefreshHeader(), refresh.getToken());
        formatter.addJsonBody(response, new JwtDto(access, refresh));
    }
}
