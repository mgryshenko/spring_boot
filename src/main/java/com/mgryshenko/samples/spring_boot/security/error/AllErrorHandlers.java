package com.mgryshenko.samples.spring_boot.security.error;

import com.mgryshenko.samples.spring_boot.security.error.format.IErrorResponseFormatter;
import com.mgryshenko.samples.spring_boot.security.error.handler.IExceptionHandlerResolver;
import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import com.mgryshenko.samples.spring_boot.security.error.handler.redirect.IErrorRedirectStrategy;
import com.mgryshenko.samples.spring_boot.security.error.model.ErrorData;
import com.mgryshenko.samples.spring_boot.security.error.model.ErrorDataBuilder;
import com.mgryshenko.samples.spring_boot.security.error.model.IErrorData;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.net.URI;
import java.util.Locale;

import static org.springframework.http.HttpStatus.*;

public class AllErrorHandlers {

    private final ErrorDataBuilder builder = new ErrorDataBuilder();

    private final ResourceBundleMessageSource msg;
    private final IErrorResponseFormatter formatter;
    private final IErrorRedirectStrategy redirectStrategy;

    public AllErrorHandlers(ResourceBundleMessageSource msg,
                            IErrorResponseFormatter formatter,
                            IErrorRedirectStrategy redirectStrategy) {
        this.msg = msg;
        this.formatter = formatter;
        this.redirectStrategy = redirectStrategy;
    }

    public void applyHandlers(IExceptionHandlerResolver resolver) {
        resolver.setDefaultHandler(this::unhandledException);
        resolver.setResponseErrorHandler(this::responseErrorException);
        resolver.addHandler(AuthenticationException.class, this::authenticationException);
        resolver.addHandler(UsernameNotFoundException.class, this::usernameNotFoundException);
        resolver.addHandler(InsufficientAuthenticationException.class, this::insufficientAuthenticationException);
        resolver.addHandler(ConstraintViolationException.class, this::constraintViolationException);
        resolver.addHandler(MethodArgumentTypeMismatchException.class, this::methodArgumentTypeMismatch);
        resolver.addHandler(BadCredentialsException.class, this::badCredentialsException);
    }

    public void unhandledException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        String message = errMsg("error.internal_server_error", request.getLocale());
        withMessage(request, response, ex, INTERNAL_SERVER_ERROR, message);
    }

    public void responseErrorException(IResponseError err, Exception ex, HttpServletRequest request, HttpServletResponse response) {
        String message = errMsg(err.getMsgKey(), request.getLocale());
        if ((err.getMsgParams() != null) && (err.getMsgParams().length > 0)) {
            message = String.format(message, err.getMsgParams());
        }
        withMessage(request, response, ex, err.getStatus(), message);
    }

    public void usernameNotFoundException(UsernameNotFoundException ex, HttpServletRequest request, HttpServletResponse response) {
        String message = String.format(errMsg("error.auth.useraccount_not_found", request.getLocale()), ex.getMessage());
        withMessage(request, response, ex, UNAUTHORIZED, message);
    }

    public void insufficientAuthenticationException(InsufficientAuthenticationException ex, HttpServletRequest request, HttpServletResponse response) {
        withMessageKey(request, response, ex, UNAUTHORIZED, "error.auth.insufficient_credentials");
    }

    public void constraintViolationException(ConstraintViolationException ex, HttpServletRequest request, HttpServletResponse response) {
        String message = errMsg("error.validation_constraints", request.getLocale());
        ErrorData data = builder.dataWithMessage(BAD_REQUEST, request, ex, message);
        data.addValidationConstraintErrors(ex.getConstraintViolations());
        formatter.configureResponse(response, data);
    }

    public void methodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, HttpServletRequest request, HttpServletResponse response) {
        String requiredType = (ex.getRequiredType() != null) ? ex.getRequiredType().getSimpleName() : null;
        String message = String.format(errMsg("error.method_argument_type_mismatch", request.getLocale()), ex.getName(), ex.getValue(), requiredType);
        withMessage(request, response, ex, BAD_REQUEST, message);
    }

    public void badCredentialsException(BadCredentialsException ex, HttpServletRequest request, HttpServletResponse response) {
        withMessageKey(request, response, ex, UNAUTHORIZED, "error.bad_credentials");
    }

    public void authenticationException(AuthenticationException ex, HttpServletRequest request, HttpServletResponse response) {
        withMessageKey(request, response, ex, UNAUTHORIZED, "error.auth.failed");
    }

    public void withMessageKey(HttpServletRequest request,
                                HttpServletResponse response,
                                Exception ex,
                                HttpStatus status,
                                String msgKey) {
        withMessage(request, response, ex, status, errMsg(msgKey, request.getLocale()));
    }

    public void withMessage(HttpServletRequest request,
                                  HttpServletResponse response,
                                  Exception ex,
                                  HttpStatus status,
                                  String message) {
        IErrorData data = builder.dataWithMessage(status, request, ex, message);
        redirectOrConfigureResponse(request, response, ex, data);
    }

    public void redirectOrConfigureResponse(HttpServletRequest request, HttpServletResponse response, Throwable ex, IErrorData data) {
        URI redirectUri = redirectStrategy.resolveRedirectUri(request, ex, data);
        if (redirectUri != null) {
            formatter.configureRedirect(response, data, redirectUri);
        } else {
            formatter.configureResponse(response, data);
        }
    }

    public String errMsg(String key, Locale locale) {
        return msg.getMessage(key, null, locale);
    }
}
