package com.mgryshenko.samples.spring_boot.security.oauth.filter;

import com.mgryshenko.samples.spring_boot.security.oauth.service.OAuth2HttpCookieResolver;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HttpCookieOAuth2AuthorizationRequestRepository implements AuthorizationRequestRepository<OAuth2AuthorizationRequest> {

    public static final String ORIGINAL_SERVLET_PATH_COOKIE = "original_uri";
    public static final String OAUTH2_REDIRECT_URI_KEY = "redirect_uri";
    public static final String OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME = "oauth2_auth_request";
    private static final int cookieExpireSeconds = 180;

    private final OAuth2HttpCookieResolver cookieResolver = new OAuth2HttpCookieResolver();

    @Override
    public OAuth2AuthorizationRequest loadAuthorizationRequest(HttpServletRequest httpServletRequest) {
        return cookieResolver.getDecodedCookie(httpServletRequest, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME, OAuth2AuthorizationRequest.class)
                .orElse(null);
    }

    @Override
    public void saveAuthorizationRequest(OAuth2AuthorizationRequest auth2AuthorizationRequest,
                                         HttpServletRequest httpServletRequest,
                                         HttpServletResponse httpServletResponse) {
        if (auth2AuthorizationRequest == null) {
            cookieResolver.deleteCookie(httpServletRequest, httpServletResponse, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME);
            cookieResolver.deleteCookie(httpServletRequest, httpServletResponse, OAUTH2_REDIRECT_URI_KEY);
            return;
        }

        cookieResolver.addCookie(httpServletResponse, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME, cookieResolver.base64encode(auth2AuthorizationRequest), cookieExpireSeconds);
        String redirectUriAfterLogin = httpServletRequest.getParameter(OAUTH2_REDIRECT_URI_KEY);
        if (StringUtils.isNotBlank(redirectUriAfterLogin)) {
            cookieResolver.addCookie(httpServletResponse, OAUTH2_REDIRECT_URI_KEY, redirectUriAfterLogin, cookieExpireSeconds);
        }
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(HttpServletRequest httpServletRequest) {
        return loadAuthorizationRequest(httpServletRequest);
    }

    public void removeAuthenticationRequestCookies(HttpServletRequest request, HttpServletResponse response) {
        cookieResolver.deleteCookie(request, response, OAUTH2_AUTHORIZATION_REQUEST_COOKIE_NAME);
        cookieResolver.deleteCookie(request, response, OAUTH2_REDIRECT_URI_KEY);
        cookieResolver.deleteCookie(request, response, ORIGINAL_SERVLET_PATH_COOKIE);
    }
}
