package com.mgryshenko.samples.spring_boot.security.cors;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationServiceException;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;

public abstract class CorsAuthenticationServiceException extends AuthenticationServiceException implements IResponseError {
    public CorsAuthenticationServiceException(String msg, Throwable t) {
        super(msg, t);
    }

    public CorsAuthenticationServiceException(String msg) {
        super(msg);
    }

    @Override
    public HttpStatus getStatus() {
        return UNAUTHORIZED;
    }

    @Override
    public Object[] getMsgParams() {
        return null;
    }
}
