package com.mgryshenko.samples.spring_boot.security.cors;

import lombok.Getter;

@Getter
public class UntrustedOriginUriException extends CorsAuthenticationServiceException {

    private final String uri;

    public UntrustedOriginUriException(String uri) {
        super("CORS policy rejected untrusted URI: " + uri);
        this.uri = uri;
    }

    @Override
    public String getMsgKey() {
        return "error.auth.unauthorized_origin_uri";
    }
}
