package com.mgryshenko.samples.spring_boot.security.jwt.filter;

import com.mgryshenko.samples.spring_boot.security.jwt.model.JwtRefreshDto;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtRefreshDetailsService;
import com.mgryshenko.samples.spring_boot.security.user.PostAuthenticationChecker;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

public class JwtRefreshAuthenticationManager implements AuthenticationManager {

    private final UserDetailsChecker userDetailsChecker = new PostAuthenticationChecker();
    private final IJwtRefreshDetailsService jwtRefreshService;

    public JwtRefreshAuthenticationManager(IJwtRefreshDetailsService jwtRefreshService) {
        this.jwtRefreshService = jwtRefreshService;
    }

    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        JwtRefreshDto tokenDto = (JwtRefreshDto) auth.getPrincipal();

        UserDetails userDetails = jwtRefreshService.findUser(tokenDto.getRefreshToken());
        userDetailsChecker.check(userDetails);

        return new PreAuthenticatedAuthenticationToken(userDetails, tokenDto, userDetails.getAuthorities());
    }
}
