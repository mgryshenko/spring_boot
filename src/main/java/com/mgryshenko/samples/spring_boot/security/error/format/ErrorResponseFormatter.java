package com.mgryshenko.samples.spring_boot.security.error.format;

import com.mgryshenko.samples.spring_boot.security.error.model.IErrorData;
import com.mgryshenko.samples.spring_boot.security.format.HttpResponseFormatter;
import com.mgryshenko.samples.spring_boot.security.format.IResponseFormatter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import static org.springframework.http.HttpStatus.FOUND;

@Slf4j
public class ErrorResponseFormatter implements IErrorResponseFormatter {

    private IResponseFormatter formatter = new HttpResponseFormatter();

    @Override
    public void setFormatter(IResponseFormatter formatter) {
        this.formatter = formatter;
    }

    @Override
    public void configureResponse(HttpServletResponse response, IErrorData data) {
        formatter.addStatus(response, data.getStatus().value());
        try {
            formatter.addJsonBody(response, data);
        } catch (IOException e) {
            log.error("Failed to write error data in response body, no data will be added", e);
        }
    }

    @Override
    public void configureRedirect(HttpServletResponse response, IErrorData data, URI redirectUri) {
        String location = UriComponentsBuilder.fromUri(redirectUri)
                .queryParam("error", encodeParam(data.getMessage()))
                .build()
                .toUriString();
        formatter.addStatus(response, FOUND.value());
        formatter.addHeader(response, HttpHeaders.LOCATION, location);
    }

    protected String encodeParam(String param) {
        try {
            return URLEncoder.encode(param, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            return param;
        }
    }
}
