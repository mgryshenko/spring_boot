package com.mgryshenko.samples.spring_boot.security.error.model;

import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;

public class ErrorDataBuilder {

//    protected final ResourceBundleMessageSource msg;
//
//    public ErrorDataFormatter(ResourceBundleMessageSource msg) {
//        this.msg = msg;
//    }
//
//    public ErrorData dataWithMessageKey(HttpStatus status, HttpServletRequest request, Exception ex, String msgKey) {
//        String message = errMsg(msgKey, request.getLocale());
//        return dataWithMessage(status, request, ex, message);
//    }
//
//    protected String errMsg(String key, Locale locale) {
//        return msg.getMessage(key, null, locale);
//    }

    public ErrorData dataWithMessage(HttpStatus status, HttpServletRequest request, Exception ex, String msg) {
        ErrorData data = new ErrorData(status);
        addDebugMessage(data, ex);
        addRequestPath(data, request);
        addMessage(data, msg);
        return data;
    }

    protected void addRequestPath(ErrorData data, HttpServletRequest request) {
        data.setPath(getServletPath(request));
    }

    protected void addMessage(ErrorData data, String msg) {
        data.setMessage(msg);
    }

    protected void addDebugMessage(ErrorData data, Throwable ex) {
        Throwable cause = ex.getCause();
        String debugMsg = (cause != null) ? cause.getMessage() : ex.getMessage();
        data.setDebugMessage(debugMsg);
    }

    protected String getServletPath(HttpServletRequest request) {
        return (request != null) ? request.getServletPath() : null;
    }
}
