package com.mgryshenko.samples.spring_boot.security.error;

import com.mgryshenko.samples.spring_boot.security.error.filter.NoRedirectAuthenticationFailureHandler;
import com.mgryshenko.samples.spring_boot.security.error.format.ErrorResponseFormatter;
import com.mgryshenko.samples.spring_boot.security.error.handler.ExceptionHandlerMappingResolver;
import com.mgryshenko.samples.spring_boot.security.error.format.IErrorResponseFormatter;
import com.mgryshenko.samples.spring_boot.security.error.handler.IExceptionHandlerResolver;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

@Configuration
@Getter
public class ErrorConfig {

    private ResourceBundleMessageSource messageSource;
    private IExceptionHandlerResolver exceptionHandlerResolver;
    private AuthenticationFailureHandler noRedirectAuthenticationFailureHandler;

    @Autowired
    public void setMessageSource(ResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Autowired
    public void setExceptionHandlerResolver(IExceptionHandlerResolver exceptionHandlerResolver) {
        this.exceptionHandlerResolver = exceptionHandlerResolver;
    }

    @Autowired
    public void setNoRedirectAuthenticationFailureHandler(AuthenticationFailureHandler noRedirectAuthenticationFailureHandler) {
        this.noRedirectAuthenticationFailureHandler = noRedirectAuthenticationFailureHandler;
    }

    @Bean
    public IErrorResponseFormatter errorResponseFormatter() {
        return new ErrorResponseFormatter();
    }

    @Bean
    public IExceptionHandlerResolver exceptionHandlerResolver() {
        return new ExceptionHandlerMappingResolver();
    }

    @Bean
    public AuthenticationFailureHandler noRedirectFailureHandler() {
        return new NoRedirectAuthenticationFailureHandler();
    }
}
