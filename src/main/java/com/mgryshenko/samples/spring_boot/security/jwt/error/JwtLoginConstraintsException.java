package com.mgryshenko.samples.spring_boot.security.jwt.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import java.io.IOException;
import org.springframework.http.HttpStatus;

public class JwtLoginConstraintsException extends RuntimeException implements IResponseError {
    public JwtLoginConstraintsException(IOException cause) {
        super("Invalid login constraints", cause);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getMsgKey() {
        return "error.auth.invalid_login_constraint";
    }

    @Override
    public Object[] getMsgParams() {
        return null;
    }
}
