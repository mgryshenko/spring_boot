package com.mgryshenko.samples.spring_boot.security.oauth.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class RegisterHttpServletRequestWrapper extends HttpServletRequestWrapper {

    private final String servletPath;

    /**
     * Constructs a request object wrapping the given request.
     *
     * @param request The request to wrap
     * @param servletPath new servletPath
     * @throws IllegalArgumentException if the request is null
     */
    public RegisterHttpServletRequestWrapper(HttpServletRequest request, String servletPath) {
        super(request);
        this.servletPath = servletPath;
    }

    @Override
    public String getServletPath() {
        return servletPath;
    }


}
