package com.mgryshenko.samples.spring_boot.security.jwt.filter;

import com.mgryshenko.samples.spring_boot.security.jwt.JwtConfig;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtRefreshDetails;
import com.mgryshenko.samples.spring_boot.security.jwt.model.JwtRefreshDto;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtAccessDetailsService;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtRefreshDetailsService;
import org.springframework.security.core.Authentication;

public class JwtRefreshSuccessHandler extends JwtLoginSuccessHandler {

    public JwtRefreshSuccessHandler(IJwtRefreshDetailsService jwtRefreshService, IJwtAccessDetailsService jwtAccessService, JwtConfig jwtConfig) {
        super(jwtRefreshService, jwtAccessService, jwtConfig);
    }

    @Override
    protected IJwtRefreshDetails provideRefreshToken(Authentication auth) {
        JwtRefreshDto refreshToken = (JwtRefreshDto) auth.getCredentials();
        return jwtRefreshService.refresh(refreshToken.getRefreshToken());
    }
}
