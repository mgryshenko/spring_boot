package com.mgryshenko.samples.spring_boot.security.error.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IExceptionHandlerResolver {
    void setDefaultHandler(IExceptionHandler<Exception> handler);
    void setResponseErrorHandler(IResponseErrorHandler handler);
    <T extends Throwable> void addHandler(Class<T> exceptionType, IExceptionHandler<T> handler);
    void handle(Exception ex, HttpServletRequest request, HttpServletResponse response);
}
