package com.mgryshenko.samples.spring_boot.security.error.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IResponseErrorHandler {
    void handle(IResponseError err, Exception base, HttpServletRequest request, HttpServletResponse response);
}
