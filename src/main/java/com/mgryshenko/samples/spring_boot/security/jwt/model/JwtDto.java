package com.mgryshenko.samples.spring_boot.security.jwt.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@JsonInclude
public class JwtDto {
    @JsonProperty("refresh_token")
    private final String refreshToken;
    @JsonProperty("refresh_exp")
    private final long refreshExpire;

    private final String token;
    private final String email;
    private final long iat;
    private final long exp;

    public JwtDto(IJwtAccessDetails access, IJwtRefreshDetails refresh) {
        refreshToken = refresh.getToken();
        refreshExpire = toSec(refresh.getExpiryDate());

        token = access.getToken();
        email = access.getEmail();
        iat = toSec(access.getIssuedDate());
        exp = toSec(access.getExpiryDate());
    }

    private long toSec(Date date) {
        return date.getTime() / 1000;
    }
}
