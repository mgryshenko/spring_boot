package com.mgryshenko.samples.spring_boot.security.error.filter;

import com.mgryshenko.samples.spring_boot.security.error.handler.IExceptionHandlerResolver;
import com.mgryshenko.samples.spring_boot.security.oauth.filter.HttpCookieOAuth2AuthorizationRequestRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class NoRedirectAuthenticationEntryPoint implements AuthenticationEntryPoint {

    protected final HttpCookieOAuth2AuthorizationRequestRepository repository = new HttpCookieOAuth2AuthorizationRequestRepository();
    protected final IExceptionHandlerResolver errorResponseHandler;

    public NoRedirectAuthenticationEntryPoint(IExceptionHandlerResolver errorResponseHandler) {
        this.errorResponseHandler = errorResponseHandler;
    }

    /**
     * Redirects unhandled {@link AuthenticationException} to "/error" path with {@link AnonymousAuthenticationToken}
     * principal for more wise configured error response
     * This should be avoided if possible, as all {@link AuthenticationException} should be caught by appropriate
     * {@link AuthenticationFailureHandler}
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) {
        log.debug("[Auth][AuthenticationEntryPoint] authorization exception: " + e.getMessage(), e);

        errorResponseHandler.handle(e, request, response);
        repository.removeAuthenticationRequestCookies(request, response);
    }
}
