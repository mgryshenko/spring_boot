package com.mgryshenko.samples.spring_boot.security.error.format;

import com.mgryshenko.samples.spring_boot.security.error.model.IErrorData;
import com.mgryshenko.samples.spring_boot.security.format.IResponseFormatter;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;

public interface IErrorResponseFormatter {
    void setFormatter(IResponseFormatter formatter);
    void configureResponse(HttpServletResponse response, IErrorData data);
    void configureRedirect(HttpServletResponse response, IErrorData data, URI redirectUri);
}
