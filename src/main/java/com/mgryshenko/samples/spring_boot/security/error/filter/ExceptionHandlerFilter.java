package com.mgryshenko.samples.spring_boot.security.error.filter;

import com.mgryshenko.samples.spring_boot.security.error.handler.IExceptionHandlerResolver;
import com.mgryshenko.samples.spring_boot.security.oauth.filter.HttpCookieOAuth2AuthorizationRequestRepository;
import java.io.IOException;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ExceptionHandlerFilter extends OncePerRequestFilter {

    protected final HttpCookieOAuth2AuthorizationRequestRepository repository = new HttpCookieOAuth2AuthorizationRequestRepository();
    protected final IExceptionHandlerResolver errorResponseHandler;

    public ExceptionHandlerFilter(IExceptionHandlerResolver errorResponseHandler) {
        this.errorResponseHandler = errorResponseHandler;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (RuntimeException ex) {
            errorResponseHandler.handle(ex, request, response);
            repository.removeAuthenticationRequestCookies(request, response);
        }
    }
}
