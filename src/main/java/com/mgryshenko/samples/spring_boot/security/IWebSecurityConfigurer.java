package com.mgryshenko.samples.spring_boot.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

public interface IWebSecurityConfigurer {
    void configure(HttpSecurity http) throws Exception;
}
