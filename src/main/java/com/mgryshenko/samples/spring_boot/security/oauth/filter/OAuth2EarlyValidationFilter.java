package com.mgryshenko.samples.spring_boot.security.oauth.filter;

import com.mgryshenko.samples.spring_boot.security.oauth.error.UnknownProviderOAuth2Exception;
import com.mgryshenko.samples.spring_boot.security.oauth.model.AuthProvider;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IAuthorizedOauth2UriService;
import java.io.IOException;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OAuth2EarlyValidationFilter extends OncePerRequestFilter {

    protected final IAuthorizedOauth2UriService authorizedUriService;

    protected RequestMatcher requestMatcher = request -> false;

    public OAuth2EarlyValidationFilter(IAuthorizedOauth2UriService authorizedUriService) {
        this.authorizedUriService = authorizedUriService;
    }

    public void setRequestMatcher(RequestMatcher requestMatcher) {
        this.requestMatcher = requestMatcher;
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        return !requestMatcher.matches(request);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        validate(request);
        filterChain.doFilter(request, response);
    }

    private void validate(HttpServletRequest request) {
        validateUri(request);
        validateProvider(request);
    }

    private void validateProvider(HttpServletRequest request) {
        String uri = request.getServletPath();
        String[] uriChunks = uri.split("/");
        for (String uriChunk : uriChunks) {
            if (AuthProvider.fromName(uriChunk) != AuthProvider.unknown) {
                return;
            }
        }
        throw new UnknownProviderOAuth2Exception(uri);
    }

    private void validateUri(HttpServletRequest request) {
        authorizedUriService.authorizeRedirectUri(request);
    }
}
