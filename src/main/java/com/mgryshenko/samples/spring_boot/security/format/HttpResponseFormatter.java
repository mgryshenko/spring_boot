package com.mgryshenko.samples.spring_boot.security.format;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HttpResponseFormatter implements IResponseFormatter {

    @Override
    public void addHeader(HttpServletResponse response, String name, String value) {
        response.addHeader(name, value);
    }

    @Override
    public void addStatus(HttpServletResponse response, int status) {
        response.setStatus(status);
    }

    @Override
    public void addJsonBody(HttpServletResponse response, Object content) throws IOException {
        ObjectWriter mapper = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = mapper.writeValueAsString(content);

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.getWriter().write(json);
    }
}
