package com.mgryshenko.samples.spring_boot.security.error.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.validation.ConstraintViolation;

@Data
@JsonTypeInfo(include = JsonTypeInfo.As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.CUSTOM, property = "error", visible = true)
@JsonTypeIdResolver(ErrorClassNameResolver.class)
public class ErrorData implements IErrorData {

    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date timestamp;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String path;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String debugMessage;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ISubErrorData> subErrors;

    private ErrorData() {
        timestamp = new Date();
    }

    public ErrorData(HttpStatus status) {
        this();
        this.status = status;
    }

    private void addSubError(ISubErrorData subError) {
        if (subErrors == null) {
            subErrors = new ArrayList<>();
        }
        subErrors.add(subError);
    }

    public void addValidationFieldErrors(List<FieldError> errors) {
        addSubErrors(errors, ValidationSubErrorData::new);
    }

    public void addValidationGlobalErrors(List<ObjectError> errors) {
        addSubErrors(errors, ValidationSubErrorData::new);
    }

    public void addValidationConstraintErrors(Set<ConstraintViolation<?>> violations) {
        addSubErrors(violations, ValidationSubErrorData::new);
    }

    private <T> void addSubErrors(Collection<T> errors, Function<T, ISubErrorData> subErrorMapper) {
        errors.forEach(e -> addSubError(subErrorMapper.apply(e)));
    }

}
