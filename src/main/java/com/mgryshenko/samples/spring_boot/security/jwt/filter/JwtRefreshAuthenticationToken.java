package com.mgryshenko.samples.spring_boot.security.jwt.filter;

import com.mgryshenko.samples.spring_boot.security.jwt.model.JwtRefreshDto;
import java.util.Collection;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class JwtRefreshAuthenticationToken implements Authentication {

    private final JwtRefreshDto token;

    public JwtRefreshAuthenticationToken(JwtRefreshDto token) {
        this.token = token;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public JwtRefreshDto getPrincipal() {
        return token;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return null;
    }
}