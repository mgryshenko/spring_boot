package com.mgryshenko.samples.spring_boot.security.oauth.filter;

import com.mgryshenko.samples.spring_boot.security.oauth.model.ISuccessRedirectToken;
import com.mgryshenko.samples.spring_boot.security.oauth.model.user.OAuth2UserInfoFactory;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IAuthorizedOauth2UriService;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IUserOAuth2DetailsService;
import com.mgryshenko.samples.spring_boot.security.oauth.service.OAuth2HttpCookieResolver;
import com.mgryshenko.samples.spring_boot.security.user.IOAuth2UserDetails;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.mgryshenko.samples.spring_boot.security.oauth.filter.HttpCookieOAuth2AuthorizationRequestRepository.ORIGINAL_SERVLET_PATH_COOKIE;
import static javax.servlet.http.HttpServletResponse.SC_CREATED;
import static javax.servlet.http.HttpServletResponse.SC_OK;

@Slf4j
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    public static final String TARGET_REDIRECT_URI = "target_redirect_uri";
    public static final String REFRESH_TOKEN_RESPONSE_PARAM = "token";

    protected final HttpCookieOAuth2AuthorizationRequestRepository repository = new HttpCookieOAuth2AuthorizationRequestRepository();
    protected final OAuth2HttpCookieResolver cookieResolver = new OAuth2HttpCookieResolver();
    protected final OAuth2UserInfoFactory userInfoFactory = new OAuth2UserInfoFactory();

    protected final IAuthorizedOauth2UriService authorizedUriService;
    protected final IUserOAuth2DetailsService service;

    protected RequestMatcher registerPathMatcher = request -> false;

    public OAuth2AuthenticationSuccessHandler(IAuthorizedOauth2UriService authorizedUriService,
                                              IUserOAuth2DetailsService service) {
        this.authorizedUriService = authorizedUriService;
        this.service = service;
    }

    public void setRegisterPathMatcher(RequestMatcher registerPathMatcher) {
        this.registerPathMatcher = registerPathMatcher;
    }

    @Override
    protected void handle(HttpServletRequest request,
                          HttpServletResponse response,
                          Authentication authentication) throws IOException, ServletException {
        processAuthenticationSuccess(request, response, (OAuth2AuthenticationToken) authentication);
        clearAuthenticationCookies(request, response);
        super.handle(request, response, authentication);
    }

    private void processAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, OAuth2AuthenticationToken auth) {
        boolean isRegisterRequest = isRegistrationRequest(request);
        IOAuth2UserDetails user = getUserInfo(auth);
        ISuccessRedirectToken successToken = isRegisterRequest ? service.register(user) : service.login(user);
        setTargetUriAttribute(request, successToken.token());
        response.setStatus(isRegisterRequest ? SC_CREATED : SC_OK);
    }

    private void setTargetUriAttribute(HttpServletRequest request, String token) {
        URI redirectUri = authorizedUriService.authorizeCookieRedirectUri(request);
        String targetUri = UriComponentsBuilder.fromUri(redirectUri)
                .queryParam(REFRESH_TOKEN_RESPONSE_PARAM, token)
                .build()
                .toUriString();
        request.setAttribute(TARGET_REDIRECT_URI, targetUri);
    }

    private boolean isRegistrationRequest(HttpServletRequest request) {
        String originalRequestPath = cookieResolver.getDecodedCookie(request, ORIGINAL_SERVLET_PATH_COOKIE, String.class)
                .orElse("");
        return registerPathMatcher.matches(new RegisterHttpServletRequestWrapper(request, originalRequestPath));
    }

    private void clearAuthenticationCookies(HttpServletRequest request, HttpServletResponse response) {
        repository.removeAuthenticationRequestCookies(request, response);
    }

    @Override
    protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {
        String targetUri = (String) request.getAttribute(TARGET_REDIRECT_URI);

        if (StringUtils.isNotBlank(targetUri)) {
            return targetUri;
        }

        return super.determineTargetUrl(request, response);
    }

    private IOAuth2UserDetails getUserInfo(OAuth2AuthenticationToken authentication) throws AuthenticationException {
        String providerId = authentication.getAuthorizedClientRegistrationId();
        Map<String, Object> attributes = authentication.getPrincipal().getAttributes();
        return userInfoFactory.getOAuth2UserInfo(providerId, attributes);
    }
}

