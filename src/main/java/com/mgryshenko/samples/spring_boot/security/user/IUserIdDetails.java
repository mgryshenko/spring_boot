package com.mgryshenko.samples.spring_boot.security.user;

public interface IUserIdDetails {
    Object getId();
}
