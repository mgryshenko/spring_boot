package com.mgryshenko.samples.spring_boot.security.jwt.model;

import java.util.Collection;
import java.util.Date;
import org.springframework.security.core.GrantedAuthority;

public interface IJwtAccessDetails {
    String getEmail();
    Collection<? extends GrantedAuthority> getAuthorities();
    String getToken();
    Date getIssuedDate();
    Date getExpiryDate();
}
