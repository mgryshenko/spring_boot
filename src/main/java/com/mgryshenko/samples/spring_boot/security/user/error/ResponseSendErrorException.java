package com.mgryshenko.samples.spring_boot.security.user.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import lombok.Getter;

import java.util.Map;
import org.springframework.http.HttpStatus;

@Getter
public class ResponseSendErrorException extends RuntimeException implements IResponseError {

    private final Map<String, Object> errorAttributes;

    public ResponseSendErrorException(Map<String, Object> errorAttributes, Throwable cause) {
        super(cause);
        this.errorAttributes = errorAttributes;
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getMsgKey() {
        return "error.servlet_filter_exception.oauth2";
    }

    @Override
    public Object[] getMsgParams() {
        return null;
    }
}
