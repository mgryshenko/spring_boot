package com.mgryshenko.samples.spring_boot.security.cors;

import lombok.Getter;

@Getter
public class MalformedUriException extends CorsAuthenticationServiceException {

    private final String uri;

    public MalformedUriException(String uri) {
        super("URI is malformed");
        this.uri = uri;
    }

    @Override
    public String getMsgKey() {
        return "error.auth.malformed_redirect_uri";
    }
}
