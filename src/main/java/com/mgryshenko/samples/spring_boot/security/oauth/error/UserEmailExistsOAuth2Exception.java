package com.mgryshenko.samples.spring_boot.security.oauth.error;

import lombok.Getter;

@Getter
public class UserEmailExistsOAuth2Exception extends OAuth2ServiceException {

    private final String email;
    private final String provider;

    public UserEmailExistsOAuth2Exception(String email, String provider) {
        super("Account for '" + email + "' is already exists. Please, use " + provider + " account to login");
        this.email = email;
        this.provider = provider;
    }

    @Override
    public String getMsgKey() {
        return "error.auth.user_email_already_exists";
    }

    @Override
    public String[] getMsgParams() {
        return new String[]{email, provider};
    }
}
