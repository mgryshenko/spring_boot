package com.mgryshenko.samples.spring_boot.security.jwt.model;

import java.util.Date;

public interface IJwtRefreshDetails {
    String getToken();
    Date getExpiryDate();
}
