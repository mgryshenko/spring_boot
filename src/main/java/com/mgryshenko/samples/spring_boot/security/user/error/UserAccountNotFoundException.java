package com.mgryshenko.samples.spring_boot.security.user.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Getter
public class UserAccountNotFoundException extends RuntimeException implements IResponseError {

    private final Long id;

    public UserAccountNotFoundException(Long id) {
        super("User not found");
        this.id = id;
    }

    @Override
    public HttpStatus getStatus() {
        return NOT_FOUND;
    }

    @Override
    public String getMsgKey() {
        return "error.useraccount_not_found";
    }

    @Override
    public Long[] getMsgParams() {
        return new Long[] {id};
    }
}
