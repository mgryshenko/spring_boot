package com.mgryshenko.samples.spring_boot.security.user.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.CONFLICT;

@Getter
public class UserEmailExistsException extends RuntimeException implements IResponseError {

    private final String email;

    public UserEmailExistsException(String email) {
        super("Username already exists");
        this.email = email;
    }

    @Override
    public HttpStatus getStatus() {
        return CONFLICT;
    }

    @Override
    public String getMsgKey() {
        return "error.user_email_already_exists";
    }

    @Override
    public String[] getMsgParams() {
        return new String[]{email};
    }
}
