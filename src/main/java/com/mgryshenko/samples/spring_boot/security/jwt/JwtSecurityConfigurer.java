package com.mgryshenko.samples.spring_boot.security.jwt;

import com.mgryshenko.samples.spring_boot.security.IWebSecurityConfigurer;
import com.mgryshenko.samples.spring_boot.security.jwt.filter.*;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtAccessDetailsService;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtRefreshDetailsService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.AuthenticationConverter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.Filter;
import java.util.function.Consumer;

@Component
public class JwtSecurityConfigurer {

    private final JwtCustomizer customizer = new JwtCustomizer(this::configure);

    private final JwtConfig jwtConfig;
    private final IJwtAccessDetailsService jwtAccessService;

    private AuthenticationManager authenticationManager;
    private IJwtRefreshDetailsService jwtRefreshService;

    @Autowired
    public JwtSecurityConfigurer(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
        jwtAccessService = jwtConfig.jwtAccessService();
    }

    public JwtCustomizer customize(AuthenticationManager authenticationManager,
                                   IJwtRefreshDetailsService jwtRefreshService) {
        Assert.notNull(authenticationManager, "AuthenticationManager required for initial user login");
        Assert.notNull(jwtRefreshService, "IJwtRefreshDetailsService required for token refresh");
        this.authenticationManager = authenticationManager;
        this.jwtRefreshService = jwtRefreshService;
        return customizer;
    }

    private void configure(HttpSecurity http) throws Exception {
            http
                .formLogin()
                    .disable()
                .logout()
                    .disable()
                .httpBasic()
                    .disable()
                .csrf()
                    .disable()
                .cors()
                    .and()
                // make sure we use stateless session; session won't be used to store user's state.
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .addFilterAfter(jwtLoginFilter(), CorsFilter.class)
                .addFilterAfter(jwtRefreshFilter(), CorsFilter.class)
                .addFilterAfter(jwtAccessFilter(), CorsFilter.class);
    }

    private Filter jwtLoginFilter() {
        AuthenticationConverter authConverter = new JwtLoginAuthenticationConverter();
        AuthenticationSuccessHandler successHandler = new JwtLoginSuccessHandler(jwtRefreshService, jwtAccessService, jwtConfig);

        AuthenticationFilter filter = new JwtLoginFilter(authenticationManager, authConverter);
        filter.setSuccessHandler(successHandler);
        customizer.login().apply(filter);
        return filter;
    }

    private Filter jwtRefreshFilter() {
        AuthenticationManager authManager = new JwtRefreshAuthenticationManager(jwtRefreshService);
        AuthenticationConverter authConverter = new JwtRefreshAuthenticationConverter();
        AuthenticationSuccessHandler successHandler = new JwtRefreshSuccessHandler(jwtRefreshService, jwtAccessService, jwtConfig);

        AuthenticationFilter filter = new JwtRefreshFilter(authManager, authConverter);
        filter.setSuccessHandler(successHandler);
        customizer.refresh().apply(filter);
        return filter;
    }

    private Filter jwtAccessFilter() {
        AuthenticationManager authManager = auth -> auth;
        AuthenticationConverter authConverter = new JwtAuthenticationConverter(jwtAccessService, jwtConfig);
        AuthenticationSuccessHandler successHandler = (request, response, authentication) -> {};

        AuthenticationFilter filter = new JwtAuthenticationFilter(authManager, authConverter);
        filter.setSuccessHandler(successHandler);
        customizer.access().apply(filter);
        return filter;
    }

    public static class JwtCustomizer {
        private final FilterCustomizer jwtLoginFilter = new FilterCustomizer(this);
        private final FilterCustomizer jwtRefreshFilter = new FilterCustomizer(this);
        private final FilterCustomizer jwtAccessFilter = new FilterCustomizer(this);

        private final IWebSecurityConfigurer configurer;

        public JwtCustomizer(IWebSecurityConfigurer configurer) {
            this.configurer = configurer;
        }

        public FilterCustomizer login() {
            return jwtLoginFilter;
        }

        public FilterCustomizer refresh() {
            return jwtRefreshFilter;
        }

        public FilterCustomizer access() {
            return jwtAccessFilter;
        }

        public FilterCustomizer login(HttpMethod method, String pathPattern) {
            return login().antMatchers(method, pathPattern);
        }

        public FilterCustomizer refresh(HttpMethod method, String pathPattern) {
            return refresh().antMatchers(method, pathPattern);
        }

        public FilterCustomizer access(HttpMethod method, String pathPattern) {
            return access().antMatchers(method, pathPattern);
        }

        public JwtCustomizer failureHandler(AuthenticationFailureHandler failureHandler) {
            jwtLoginFilter.failureHandler(failureHandler);
            jwtRefreshFilter.failureHandler(failureHandler);
            jwtAccessFilter.failureHandler(failureHandler);
            return this;
        }

        public void apply(HttpSecurity http) throws Exception {
            configurer.configure(http);
        }
    }

    public static class FilterCustomizer {
        private RequestMatcher antMatchers;
        private AuthenticationFailureHandler failureHandler;
        private AuthenticationSuccessHandler successHandler;

        private final JwtCustomizer parent;

        public FilterCustomizer(JwtCustomizer parent) {
            this.parent = parent;
        }

        public FilterCustomizer antMatchers(HttpMethod method, String pathPattern) {
            antMatchers = new AntPathRequestMatcher(pathPattern, method.name());
            return this;
        }

        public FilterCustomizer failureHandler(AuthenticationFailureHandler failureHandler) {
            this.failureHandler = failureHandler;
            return this;
        }

        public FilterCustomizer successHandler(AuthenticationSuccessHandler successHandler) {
            this.successHandler = successHandler;
            return this;
        }

        private void apply(AuthenticationFilter filter) {
            applyIfNotNull(filter::setRequestMatcher, antMatchers);
            applyIfNotNull(filter::setSuccessHandler, successHandler);
            applyIfNotNull(filter::setFailureHandler, failureHandler);
        }

        private <R> void applyIfNotNull(Consumer<R> consumer, R arg) {
            if (arg != null) {
                consumer.accept(arg);
            }
        }

        public JwtCustomizer and() {
            return parent;
        }
    }
}
