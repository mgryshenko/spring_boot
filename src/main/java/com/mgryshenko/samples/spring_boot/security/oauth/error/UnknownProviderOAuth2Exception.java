package com.mgryshenko.samples.spring_boot.security.oauth.error;

import lombok.Getter;

@Getter
public class UnknownProviderOAuth2Exception extends OAuth2ServiceException {

    String providerId;

    public UnknownProviderOAuth2Exception(String providerId) {
        super("Social profile provider '" + providerId + "' is invalid or not supported yet");
        this.providerId = providerId;
    }

    @Override
    public String getMsgKey() {
        return "error.auth.unknown_provider";
    }
}
