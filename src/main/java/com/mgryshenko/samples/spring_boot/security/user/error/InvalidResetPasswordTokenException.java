package com.mgryshenko.samples.spring_boot.security.user.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class InvalidResetPasswordTokenException extends RuntimeException implements IResponseError {

    public InvalidResetPasswordTokenException() {
        super("Bad token: expired or doesn't exist");
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getMsgKey() {
        return "error.bad_reset_password_token";
    }

    @Override
    public Object[] getMsgParams() {
        return null;
    }
}
