package com.mgryshenko.samples.spring_boot.security.oauth.model.user;

import com.mgryshenko.samples.spring_boot.security.oauth.model.AuthProvider;
import com.mgryshenko.samples.spring_boot.security.user.IOAuth2UserDetails;
import java.util.Map;
import org.springframework.security.authentication.InternalAuthenticationServiceException;

public class OAuth2UserInfoFactory {

    public IOAuth2UserDetails getOAuth2UserInfo(String registrationId, Map<String, Object> attributes) {
        AuthProvider provider = AuthProvider.fromName(registrationId);
        switch (provider) {
            case google:
                return new GoogleOAuth2UserInfo(attributes);
            case facebook:
                return new FacebookOAuth2UserInfo(attributes);
            case github:
                return new GithubOAuth2UserInfo(attributes);
        }
        throw new InternalAuthenticationServiceException("Login with " + registrationId + " not supported yet");
    }
}
