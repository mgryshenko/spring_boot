package com.mgryshenko.samples.spring_boot.security.jwt.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailPasswordDto {

    @NotNull
    @NotEmpty
    private String email;
    private String password;

    @JsonCreator
    public EmailPasswordDto(@JsonProperty(value = "email", required = true) String email,
                            @JsonProperty(value = "password", required = true) String password) {
        this.email = email;
        this.password = password;
    }
}
