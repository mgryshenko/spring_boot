package com.mgryshenko.samples.spring_boot.security.jwt.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgryshenko.samples.spring_boot.security.jwt.error.JwtLoginConstraintsException;
import com.mgryshenko.samples.spring_boot.security.jwt.model.EmailPasswordDto;
import java.io.IOException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationConverter;

import javax.servlet.http.HttpServletRequest;

public class JwtLoginAuthenticationConverter implements AuthenticationConverter {
    @Override
    public Authentication convert(HttpServletRequest request) {
        try {
            EmailPasswordDto user = new ObjectMapper().readValue(request.getInputStream(), EmailPasswordDto.class);
            return new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());
        } catch (IOException e) {
            throw new JwtLoginConstraintsException(e);
        }
    }
}