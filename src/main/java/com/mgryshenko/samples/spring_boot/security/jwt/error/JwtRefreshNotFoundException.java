package com.mgryshenko.samples.spring_boot.security.jwt.error;

public class JwtRefreshNotFoundException extends JwtRefreshValidationException {
    public JwtRefreshNotFoundException() {
        super("Refresh JWT not found");
    }
}
