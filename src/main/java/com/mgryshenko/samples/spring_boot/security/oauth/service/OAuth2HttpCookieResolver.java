package com.mgryshenko.samples.spring_boot.security.oauth.service;

import java.util.Base64;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.SerializationUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OAuth2HttpCookieResolver {

    public <T> Optional<T> getDecodedCookie(HttpServletRequest request, String name, Class<T> clazz) {
        Optional<Cookie> cookie = getCookie(request, name);
        return cookie.filter(c -> StringUtils.isNotBlank(c.getValue()))
                .map(c -> base64decode(c, clazz));
    }

    public Optional<Cookie> getCookie(HttpServletRequest request, String name) {
        if (StringUtils.isBlank(name)) {
            return Optional.empty();
        }

        Cookie[] cookies = request.getCookies();

        if ((cookies != null) && (cookies.length > 0)) {
            for (Cookie cookie : cookies) {
                if (name.equals(cookie.getName())) {
                    return Optional.of(cookie);
                }
            }
        }
        return Optional.empty();
    }


    public void deleteCookie(HttpServletRequest request, HttpServletResponse response, String name) {
        getCookie(request, name).ifPresent(cookie -> {
            cookie.setValue("");
            cookie.setPath("/");
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        });
    }

    public <T> T base64decode(Cookie cookie, Class<T> clazz) {
        return clazz.cast(SerializationUtils.deserialize(
                    Base64.getUrlDecoder().decode(cookie.getValue())
        ));
    }

    public String base64encode(Object object) {
        return Base64.getUrlEncoder().encodeToString(SerializationUtils.serialize(object));
    }

    public void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
    }
}
