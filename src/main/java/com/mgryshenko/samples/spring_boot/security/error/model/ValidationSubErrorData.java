package com.mgryshenko.samples.spring_boot.security.error.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.validation.ConstraintViolation;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class ValidationSubErrorData implements ISubErrorData {

    private String object;
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String field;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object rejectedValue;

    public ValidationSubErrorData(ObjectError error) {
        object = error.getObjectName();
        message = error.getDefaultMessage();
    }

    public ValidationSubErrorData(FieldError error) {
        this(error.getObjectName(),
                error.getDefaultMessage(),
                error.getField(),
                error.getRejectedValue());
    }

    public ValidationSubErrorData(ConstraintViolation<?> cv) {
        this(cv.getRootBeanClass().getSimpleName(),
                cv.getMessage(),
                ((PathImpl) cv.getPropertyPath()).getLeafNode().asString(),
                cv.getInvalidValue());
    }
}
