package com.mgryshenko.samples.spring_boot.security.error.handler;

import org.springframework.http.HttpStatus;

public interface IResponseError {
    HttpStatus getStatus();
    String getMsgKey();
    Object[] getMsgParams();
}
