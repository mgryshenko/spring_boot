package com.mgryshenko.samples.spring_boot.security.user.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import org.springframework.http.HttpStatus;

public class InvalidOldPasswordException extends RuntimeException implements IResponseError {

    public InvalidOldPasswordException() {
        super("Provided wrong old password");
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    public String getMsgKey() {
        return "error.invalid_old_password";
    }

    @Override
    public Object[] getMsgParams() {
        return null;
    }
}
