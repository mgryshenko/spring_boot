package com.mgryshenko.samples.spring_boot.security.jwt.service;

import com.mgryshenko.samples.spring_boot.security.jwt.error.JwtRefreshValidationException;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtRefreshDetails;
import com.mgryshenko.samples.spring_boot.security.user.IUserIdDetails;
import org.springframework.security.core.userdetails.UserDetails;

public interface IJwtRefreshDetailsService {
    IJwtRefreshDetails create(IUserIdDetails user);
    IJwtRefreshDetails refresh(String token);
    UserDetails findUser(String token) throws JwtRefreshValidationException;
}
