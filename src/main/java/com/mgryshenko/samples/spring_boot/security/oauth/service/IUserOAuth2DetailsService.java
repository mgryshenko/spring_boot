package com.mgryshenko.samples.spring_boot.security.oauth.service;

import com.mgryshenko.samples.spring_boot.security.oauth.model.ISuccessRedirectToken;
import com.mgryshenko.samples.spring_boot.security.user.IOAuth2UserDetails;

public interface IUserOAuth2DetailsService {
    ISuccessRedirectToken login(IOAuth2UserDetails user);
    ISuccessRedirectToken register(IOAuth2UserDetails user);
}
