package com.mgryshenko.samples.spring_boot.security.user;

import com.mgryshenko.samples.spring_boot.security.oauth.model.AuthProvider;

public interface IOAuth2UserDetails {
    String getName();
    String getEmail();
    String getImageUrl();
    String getProviderId();
    AuthProvider getProvider();
}
