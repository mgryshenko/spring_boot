package com.mgryshenko.samples.spring_boot.security.error.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ExceptionDepthComparator;
import org.springframework.lang.Nullable;
import org.springframework.util.ConcurrentReferenceHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * see {@link org.springframework.web.method.annotation.ExceptionHandlerMethodResolver}
 */
@Slf4j
public class ExceptionHandlerMappingResolver implements IExceptionHandlerResolver {

    private final Map<Class<? extends Throwable>, IExceptionHandler> mappedHandlers = new HashMap<>(16);

    private final Map<Class<? extends Throwable>, IExceptionHandler> exceptionLookupCache = new ConcurrentReferenceHashMap<>(16);

    private IExceptionHandler<Exception> defaultHandler;
    private IResponseErrorHandler responseErrorHandler;

    @Override
    public void setDefaultHandler(IExceptionHandler<Exception> defaultHandler) {
        this.defaultHandler = defaultHandler;
    }

    @Override
    public void setResponseErrorHandler(IResponseErrorHandler responseErrorHandler) {
        this.responseErrorHandler = responseErrorHandler;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handle(Exception ex, HttpServletRequest request, HttpServletResponse response) {
        if ((ex instanceof IResponseError) && (responseErrorHandler != null)) {
            responseErrorHandler.handle((IResponseError) ex, ex, request, response);
            return;
        }
        IExceptionHandler handler = resolveHandler(ex);
        if (handler != null) {
            handler.handle(ex, request, response);
        } else if (defaultHandler != null){
            defaultHandler.handle(ex, request, response);
        } else {
            log.debug("Unhandled exception: " + ex.getMessage());
        }
    }

    @Override
    public <T extends Throwable> void addHandler(Class<T> exceptionType, IExceptionHandler<T> handler) {
        exceptionLookupCache.clear();
        IExceptionHandler oldHandler = mappedHandlers.put(exceptionType, handler);
        if ((oldHandler != null) && !oldHandler.equals(handler)) {
            throw new IllegalStateException("Ambiguous IExceptionHandler handler mapped for [" +
                    exceptionType + "]: {" + oldHandler + ", " + handler + "}");
        }
    }

    /**
     * Find a {@link IExceptionHandler} to handle the given exception.
     * Use {@link ExceptionDepthComparator} if more than one match is found.
     * @param exception the exception
     * @return a IExceptionHandler to handle the exception, or {@code null} if none found
     */
    @Nullable
    protected IExceptionHandler resolveHandler(Exception exception) {
        return resolveHandlerByThrowable(exception);
    }

    /**
     * Find a {@link IExceptionHandler} to handle the given Throwable.
     * Use {@link ExceptionDepthComparator} if more than one match is found.
     * @param exception the exception
     * @return a IExceptionHandler to handle the exception, or {@code null} if none found
     * @since 5.0
     */
    @Nullable
    protected IExceptionHandler resolveHandlerByThrowable(Throwable exception) {
        IExceptionHandler handler = resolveHandlerByExceptionType(exception.getClass());
        if (handler == null) {
            Throwable cause = exception.getCause();
            if (cause != null) {
                handler = resolveHandlerByExceptionType(cause.getClass());
            }
        }
        return handler;
    }

    /**
     * Find a {@link IExceptionHandler} to handle the given exception type. This can be
     * useful if an {@link Exception} instance is not available (e.g. for tools).
     * @param exceptionType the exception type
     * @return a IExceptionHandler to handle the exception, or {@code null} if none found
     */
    @Nullable
    protected IExceptionHandler resolveHandlerByExceptionType(Class<? extends Throwable> exceptionType) {
        IExceptionHandler handler = exceptionLookupCache.get(exceptionType);
        if (handler == null) {
            handler = getMappedHandler(exceptionType);
            exceptionLookupCache.put(exceptionType, handler);
        }
        return handler;
    }

    /**
     * Return the {@link IExceptionHandler} mapped to the given exception type, or {@code null} if none.
     */
    @Nullable
    protected IExceptionHandler getMappedHandler(Class<? extends Throwable> exceptionType) {
        List<Class<? extends Throwable>> matches = new ArrayList<>();
        for (Class<? extends Throwable> mappedException : mappedHandlers.keySet()) {
            if (mappedException.isAssignableFrom(exceptionType)) {
                matches.add(mappedException);
            }
        }
        if (!matches.isEmpty()) {
            matches.sort(new ExceptionDepthComparator(exceptionType));
            return mappedHandlers.get(matches.get(0));
        }
        else {
            return null;
        }
    }
}
