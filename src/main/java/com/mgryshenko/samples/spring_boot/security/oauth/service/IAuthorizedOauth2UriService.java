package com.mgryshenko.samples.spring_boot.security.oauth.service;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

public interface IAuthorizedOauth2UriService {
    URI authorizeCookieRedirectUri(HttpServletRequest request);
    URI authorizeRedirectUri(HttpServletRequest request);
}
