package com.mgryshenko.samples.spring_boot.security.oauth.model.user;

import com.mgryshenko.samples.spring_boot.security.user.IOAuth2UserDetails;
import java.util.Map;
import lombok.Getter;

@Getter
public abstract class OAuth2UserInfo implements IOAuth2UserDetails {
    protected Map<String, Object> attributes;

    public OAuth2UserInfo(Map<String, Object> attributes) {
        this.attributes = attributes;
    }
}
