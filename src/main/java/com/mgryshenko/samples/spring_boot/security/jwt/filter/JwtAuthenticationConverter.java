package com.mgryshenko.samples.spring_boot.security.jwt.filter;

import com.mgryshenko.samples.spring_boot.security.jwt.JwtConfig;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtAccessDetails;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtAccessDetailsService;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationConverter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import javax.servlet.http.HttpServletRequest;

import static org.apache.commons.lang3.StringUtils.removeStart;

public class JwtAuthenticationConverter implements AuthenticationConverter {

    protected final JwtConfig jwtConfig;
    protected final IJwtAccessDetailsService jwtAccessService;

    public JwtAuthenticationConverter(IJwtAccessDetailsService jwtAccessService, JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
        this.jwtAccessService = jwtAccessService;
    }

    @Override
    public Authentication convert(HttpServletRequest request) {
        String header = request.getHeader(jwtConfig.getHeader());
        if ((header == null) || !header.startsWith(jwtConfig.getSchemePrefix())) {
            return null;
        }

        String token = removeStart(header, jwtConfig.getSchemePrefix()).trim();
        IJwtAccessDetails model = jwtAccessService.parse(token);
        if (model.getEmail() != null) {
            // No need to authenticate the user - JWT claims are trusted source of GrantedAuthority list
            return new PreAuthenticatedAuthenticationToken(model.getEmail(), null, model.getAuthorities());
        }
        return null;
    }
}

