package com.mgryshenko.samples.spring_boot.security.jwt;

import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtAccessDetailsService;
import com.mgryshenko.samples.spring_boot.security.jwt.service.JwtAccessDetailsService;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:jwt.properties")
@Getter
public class JwtConfig {

    @Value("${security.jwt.header}")
    private String header;

    @Value("${security.jwt.refresh_header}")
    private String refreshHeader;

    @Value("${security.jwt.scheme_prefix}")
    private String schemePrefix;

    @Value("${security.jwt.expiration_sec}")
    private int expirationSec;

    @Value("${security.jwt.secret}")
    private String secret;

    @Bean
    public IJwtAccessDetailsService jwtAccessService() {
        return new JwtAccessDetailsService(expirationSec, secret, SignatureAlgorithm.HS512);
    }
}