package com.mgryshenko.samples.spring_boot.security.error;

import com.mgryshenko.samples.spring_boot.security.IWebSecurityConfigurer;
import com.mgryshenko.samples.spring_boot.security.cors.IAuthorizedOriginUriService;
import com.mgryshenko.samples.spring_boot.security.error.filter.ExceptionHandlerFilter;
import com.mgryshenko.samples.spring_boot.security.error.filter.NoRedirectAuthenticationEntryPoint;
import com.mgryshenko.samples.spring_boot.security.error.format.ErrorResponseFormatter;
import com.mgryshenko.samples.spring_boot.security.error.format.IErrorResponseFormatter;
import com.mgryshenko.samples.spring_boot.security.error.handler.redirect.ErrorRedirectStrategyChain;
import com.mgryshenko.samples.spring_boot.security.error.handler.redirect.IErrorRedirectStrategy;
import com.mgryshenko.samples.spring_boot.security.error.handler.redirect.OAuth2ErrorRedirectStrategy;
import com.mgryshenko.samples.spring_boot.security.format.IResponseFormatter;
import com.mgryshenko.samples.spring_boot.security.oauth.service.AuthorizedOauth2UriService;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IAuthorizedOauth2UriService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CorsFilter;

@Component
public class ErrorResponseConfigurer {

    private final ErrorCustomizer customizer = new ErrorCustomizer(this::configure);
    private final IErrorResponseFormatter simpleFormatter = new ErrorResponseFormatter();
    private final IErrorRedirectStrategy noRedirectStrategy = (request, ex, data) -> null;

    private final ErrorConfig errorConfig;

    @Autowired
    public ErrorResponseConfigurer(ErrorConfig errorConfig) {
        this.errorConfig = errorConfig;
    }

    public ErrorCustomizer customize() {
        return customizer;
    }

    private void configure(HttpSecurity http) throws Exception {
        applySecurityErrorHandlers();
            http
                .exceptionHandling()
                    .authenticationEntryPoint(noRedirectEntryPoint())
                    .and()
                .addFilterBefore(exceptionHandlerFilter(), CorsFilter.class);
    }

    private void applySecurityErrorHandlers() {
        AllErrorHandlers allSecurityErrorHandlers = new AllErrorHandlers(
                errorConfig.getMessageSource(),
                getOrDefault(customizer.getFormatter(), simpleFormatter),
                getOrDefault(customizer.getRedirectStrategy(), noRedirectStrategy)
        );
        allSecurityErrorHandlers.applyHandlers(errorConfig.getExceptionHandlerResolver());
    }

    private ExceptionHandlerFilter exceptionHandlerFilter() {
        return new ExceptionHandlerFilter(errorConfig.getExceptionHandlerResolver());
    }

    private AuthenticationEntryPoint noRedirectEntryPoint() {
        return new NoRedirectAuthenticationEntryPoint(errorConfig.getExceptionHandlerResolver());
    }

    private <T> T getOrDefault(T nullableValue, T defaultValue) {
        return (nullableValue == null) ? defaultValue : nullableValue;
    }

    public class ErrorCustomizer {
        private final ErrorRedirectStrategyChain redirectStrategyChain = new ErrorRedirectStrategyChain();

        private final IWebSecurityConfigurer configurer;

        private final ErrorFormatterCustomizer formatterCustomizer;

        public ErrorCustomizer(IWebSecurityConfigurer configurer) {
            this.configurer = configurer;
            formatterCustomizer = new ErrorFormatterCustomizer(this);
        }

        public ErrorCustomizer addRedirectStrategy(IErrorRedirectStrategy strategy) {
            redirectStrategyChain.add(strategy);
            return this;
        }

        public ErrorCustomizer addOAuth2RedirectStrategy(IAuthorizedOriginUriService authorizedOriginUriService, String baseUri) {
            IAuthorizedOauth2UriService redirectUriService = new AuthorizedOauth2UriService(authorizedOriginUriService);
            return addRedirectStrategy(new OAuth2ErrorRedirectStrategy(redirectUriService, baseUri));
        }

        public ErrorFormatterCustomizer formatter() {
            return formatterCustomizer;
        }

        protected IErrorResponseFormatter getFormatter() {
            return formatterCustomizer.getErrorFormatter();
        }

        protected IErrorRedirectStrategy getRedirectStrategy() {
            return redirectStrategyChain;
        }

        public void apply(HttpSecurity http) throws Exception {
            configurer.configure(http);
        }
    }

    public class ErrorFormatterCustomizer {
        private final ErrorCustomizer parent;

        private IErrorResponseFormatter errorFormatter;

        protected ErrorFormatterCustomizer(ErrorCustomizer parent) {
            this.parent = parent;
        }

        public ErrorFormatterCustomizer error(IErrorResponseFormatter formatter) {
            errorFormatter = formatter;
            return this;
        }

        public ErrorFormatterCustomizer response(IResponseFormatter formatter) {
            errorFormatter.setFormatter(formatter);
            return this;
        }

        protected IErrorResponseFormatter getErrorFormatter() {
            return errorFormatter;
        }

        public ErrorCustomizer and() {
            return parent;
        }
    }
}
