package com.mgryshenko.samples.spring_boot.security.jwt.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;

public abstract class JwtAuthenticationException extends AuthenticationException implements IResponseError {
    public JwtAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public JwtAuthenticationException(String msg) {
        super(msg);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.UNAUTHORIZED;
    }

    @Override
    public Object[] getMsgParams() {
        return null;
    }
}
