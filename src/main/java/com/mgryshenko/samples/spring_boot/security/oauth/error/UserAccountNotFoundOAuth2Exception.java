package com.mgryshenko.samples.spring_boot.security.oauth.error;

import lombok.Getter;

@Getter
public class UserAccountNotFoundOAuth2Exception extends OAuth2ServiceException {

    private final String username;

    public UserAccountNotFoundOAuth2Exception(String username) {
        super("No account exists for '" + username + "'. Please register first");
        this.username = username;
    }

    @Override
    public String getMsgKey() {
        return "error.auth.useraccount_not_found";
    }

    @Override
    public String[] getMsgParams() {
        return new String[]{username};
    }
}
