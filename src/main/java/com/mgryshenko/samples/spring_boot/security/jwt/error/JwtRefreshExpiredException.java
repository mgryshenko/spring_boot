package com.mgryshenko.samples.spring_boot.security.jwt.error;

public class JwtRefreshExpiredException extends JwtRefreshValidationException {
    public JwtRefreshExpiredException() {
        super("Refresh JWT expired");
    }
}
