package com.mgryshenko.samples.spring_boot.security.oauth.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationServiceException;

public abstract class OAuth2ServiceException extends AuthenticationServiceException implements IResponseError {
    public OAuth2ServiceException(String msg) {
        super(msg);
    }

    public OAuth2ServiceException(String msg, Throwable t) {
        super(msg, t);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.UNAUTHORIZED;
    }

    @Override
    public Object[] getMsgParams() {
        return null;
    }
}
