package com.mgryshenko.samples.spring_boot.security.oauth.error;

import lombok.Getter;

@Getter
public class UnexpectedProviderOAuth2Exception extends OAuth2ServiceException {

    private final String actualProvider;
    private final String expectedProvider;

    public UnexpectedProviderOAuth2Exception(String actualProvider, String expectedProvider) {
        super("Please use \"" + expectedProvider + "\" account to login");
        this.actualProvider = actualProvider;
        this.expectedProvider = expectedProvider;
    }

    @Override
    public String getMsgKey() {
        return "error.auth.unexpected_provider";
    }

    @Override
    public String[] getMsgParams() {
        return new String[]{expectedProvider};
    }
}
