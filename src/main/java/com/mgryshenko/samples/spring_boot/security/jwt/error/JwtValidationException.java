package com.mgryshenko.samples.spring_boot.security.jwt.error;

import com.mgryshenko.samples.spring_boot.security.error.handler.IResponseError;

public class JwtValidationException extends JwtAuthenticationException implements IResponseError {
    public JwtValidationException(Throwable ex) {
        super("JWT provided is not valid or expired", ex);
    }

    @Override
    public String getMsgKey() {
        return "error.auth.failed_jwt";
    }
}
