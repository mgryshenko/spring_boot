package com.mgryshenko.samples.spring_boot.security.oauth.model;

public interface ISuccessRedirectToken {
    String token();
}
