package com.mgryshenko.samples.spring_boot.security.oauth;

import com.mgryshenko.samples.spring_boot.security.IWebSecurityConfigurer;
import com.mgryshenko.samples.spring_boot.security.cors.IAuthorizedOriginUriService;
import com.mgryshenko.samples.spring_boot.security.oauth.filter.HttpCookieOAuth2AuthorizationRequestRepository;
import com.mgryshenko.samples.spring_boot.security.oauth.filter.OAuth2AuthenticationSuccessHandler;
import com.mgryshenko.samples.spring_boot.security.oauth.filter.OAuth2EarlyValidationFilter;
import com.mgryshenko.samples.spring_boot.security.oauth.filter.OAuth2RegisterRedirectFilter;
import com.mgryshenko.samples.spring_boot.security.oauth.service.AuthorizedOauth2UriService;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IAuthorizedOauth2UriService;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IUserOAuth2DetailsService;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2LoginConfigurer;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizationRequestRedirectFilter;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import java.util.function.Consumer;

import static org.springframework.http.HttpMethod.GET;

@Component
public class OAuthSecurityConfigurer {
    public static final String ROOT_URI_PATH = "/oauth2/**";
    public static final String REGISTER_URI_PATH = "/oauth2/register/*";
    public static final String CALLBACK_URI_PATH = "/oauth2/callback/*";
    public static final String AUTHENTICATE_URI_PATH = "/oauth2/authorize";

    private final AuthorizationRequestRepository<OAuth2AuthorizationRequest> authRequestRepository = new HttpCookieOAuth2AuthorizationRequestRepository();
    private final OAuthCustomizer customizer = new OAuthCustomizer(this::configure);

    private AuthenticationSuccessHandler successHandler;
    private IAuthorizedOauth2UriService authorizedUriService;

    public OAuthCustomizer customize(IAuthorizedOriginUriService authorizedOriginUriService,
                                     IUserOAuth2DetailsService userOAuth2DetailsService) {
        AuthorizedOauth2UriService uriService = new AuthorizedOauth2UriService(authorizedOriginUriService);
        successHandler = new OAuth2AuthenticationSuccessHandler(uriService, userOAuth2DetailsService);
        authorizedUriService = uriService;
        return customizer;
    }

    private void configure(HttpSecurity http) throws Exception {
            http
                .addFilterBefore(oauth2RegisterRedirectFilter(), OAuth2AuthorizationRequestRedirectFilter.class)
                .addFilterBefore(oauth2ValidationFilter(), OAuth2RegisterRedirectFilter.class)
                .oauth2Login()
                    .authorizationEndpoint()
                        .baseUri(AUTHENTICATE_URI_PATH)
                        .authorizationRequestRepository(authRequestRepository)
                        .and()
                    .redirectionEndpoint()
                        .baseUri(CALLBACK_URI_PATH)
                        .and()
                    .successHandler(successHandler());
            customizer.configure(http.oauth2Login());
    }

    public Filter oauth2RegisterRedirectFilter() {
        OAuth2RegisterRedirectFilter filter =  new OAuth2RegisterRedirectFilter();
        filter.setCallbackRequestPath(customizer.callback().getBaseUri(CALLBACK_URI_PATH), GET);
        filter.setRegisterRequestPath(customizer.registration().getBaseUri(REGISTER_URI_PATH),
                customizer.authorization().getBaseUri(AUTHENTICATE_URI_PATH), GET);
        return filter;
    }

    public Filter oauth2ValidationFilter() {
        OAuth2EarlyValidationFilter filter =  new OAuth2EarlyValidationFilter(authorizedUriService);
        filter.setRequestMatcher(new AntPathRequestMatcher(customizer.root().getBaseUri(ROOT_URI_PATH), GET.name()));
        return filter;
    }

    private AuthenticationSuccessHandler successHandler() {
        RequestMatcher registerMatcher = new AntPathRequestMatcher(customizer.registration().getBaseUri(REGISTER_URI_PATH), GET.name());
        ((OAuth2AuthenticationSuccessHandler) successHandler).setRegisterPathMatcher(registerMatcher);
        return successHandler;
    }

    public static class OAuthCustomizer {
        private final EndpointCustomizer root = new EndpointCustomizer(this);
        private final EndpointCustomizer callback = new EndpointCustomizer(this);
        private final EndpointCustomizer registration = new EndpointCustomizer(this);
        private final EndpointCustomizer authorization = new EndpointCustomizer(this);

        private final IWebSecurityConfigurer configurer;

        private AuthenticationSuccessHandler successHandler;
        private AuthenticationFailureHandler failureHandler;

        public OAuthCustomizer(IWebSecurityConfigurer configurer) {
            this.configurer = configurer;
        }

        public EndpointCustomizer root() {
            return root;
        }

        public EndpointCustomizer callback() {
            return callback;
        }

        public EndpointCustomizer registration() {
            return registration;
        }

        public EndpointCustomizer authorization() {
            return authorization;
        }

        public OAuthCustomizer successHandler(AuthenticationSuccessHandler successHandler) {
            this.successHandler = successHandler;
            return this;
        }

        public OAuthCustomizer failureHandler(AuthenticationFailureHandler failureHandler) {
            this.failureHandler = failureHandler;
            return this;
        }

        public void apply(HttpSecurity http) throws Exception {
            configurer.configure(http);
        }

        protected void configure(OAuth2LoginConfigurer<HttpSecurity> oauth2config) {
            applyIfNotNull(oauth2config.authorizationEndpoint()::baseUri, authorization.getBaseUri(null));
            applyIfNotNull(oauth2config.redirectionEndpoint()::baseUri, callback.getBaseUri(null));
            applyIfNotNull(oauth2config::successHandler, successHandler);
            applyIfNotNull(oauth2config::failureHandler, failureHandler);
        }

        private <R> void applyIfNotNull(Consumer<R> consumer, R arg) {
            if (arg != null) {
                consumer.accept(arg);
            }
        }
    }

    public static class EndpointCustomizer {
        private final OAuthCustomizer parent;

        private String baseUri;

        public EndpointCustomizer(OAuthCustomizer parent) {
            this.parent = parent;
        }

        public EndpointCustomizer baseUri(String baseUri) {
            this.baseUri = baseUri;
            return this;
        }

        protected String getBaseUri(String defaultValue) {
            return (baseUri == null) ? defaultValue : baseUri;
        }

        public OAuthCustomizer and() {
            return parent;
        }
    }
}
