package com.mgryshenko.samples.spring_boot.security.error.handler.redirect;

import com.mgryshenko.samples.spring_boot.security.error.model.IErrorData;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

public interface IErrorRedirectStrategy {
    URI resolveRedirectUri(HttpServletRequest request, Throwable ex, IErrorData data);
}
