package com.mgryshenko.samples.spring_boot.security.jwt.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgryshenko.samples.spring_boot.security.jwt.error.JwtRefreshValidationException;
import com.mgryshenko.samples.spring_boot.security.jwt.model.JwtRefreshDto;
import org.springframework.security.web.authentication.AuthenticationConverter;

import javax.servlet.http.HttpServletRequest;

public class JwtRefreshAuthenticationConverter implements AuthenticationConverter {

    @Override
    public JwtRefreshAuthenticationToken convert(HttpServletRequest request) {
        try {
            JwtRefreshDto tokenDto = new ObjectMapper().readValue(request.getInputStream(), JwtRefreshDto.class);
            return new JwtRefreshAuthenticationToken(tokenDto);
        } catch (Exception e) {
            throw new JwtRefreshValidationException();
        }
    }
}
