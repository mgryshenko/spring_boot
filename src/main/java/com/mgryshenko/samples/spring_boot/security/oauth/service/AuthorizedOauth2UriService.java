package com.mgryshenko.samples.spring_boot.security.oauth.service;

import com.mgryshenko.samples.spring_boot.security.cors.IAuthorizedOriginUriService;
import com.mgryshenko.samples.spring_boot.security.cors.MalformedUriException;
import com.mgryshenko.samples.spring_boot.security.cors.UntrustedOriginUriException;
import java.net.URI;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import static com.mgryshenko.samples.spring_boot.security.oauth.filter.HttpCookieOAuth2AuthorizationRequestRepository.OAUTH2_REDIRECT_URI_KEY;

public class AuthorizedOauth2UriService implements IAuthorizedOauth2UriService {
    protected final OAuth2HttpCookieResolver cookieResolver = new OAuth2HttpCookieResolver();

    protected final IAuthorizedOriginUriService authorizedOriginUriService;

    public AuthorizedOauth2UriService(IAuthorizedOriginUriService authorizedOriginUriService) {
        this.authorizedOriginUriService = authorizedOriginUriService;
    }

    public URI authorizeUri(String uri) {
        return authorizedOriginUriService.authorizeUri(uri);
    }

    @Override
    public URI authorizeCookieRedirectUri(HttpServletRequest request) throws UntrustedOriginUriException, MalformedUriException {
        return authorizeUri(getRedirectUriFromCookie(request));
    }

    @Override
    public URI authorizeRedirectUri(HttpServletRequest request) {
        String redirectUri = getRedirectUriFromQuery(request);
        if (StringUtils.isNotBlank(redirectUri)) {
            return authorizeUri(redirectUri);
        } else {
            return authorizeCookieRedirectUri(request);
        }
    }

    protected String getRedirectUriFromQuery(HttpServletRequest oauth2request) {
        return oauth2request.getParameter(OAUTH2_REDIRECT_URI_KEY);
    }

    private String getRedirectUriFromCookie(HttpServletRequest request) throws MalformedUriException {
        return cookieResolver.getCookie(request, OAUTH2_REDIRECT_URI_KEY)
                .map(Cookie::getValue)
                .filter(StringUtils::isNotBlank)
                .orElseThrow(() -> new MalformedUriException(""));
    }
}
