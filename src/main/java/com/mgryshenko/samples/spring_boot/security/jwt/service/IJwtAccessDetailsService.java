package com.mgryshenko.samples.spring_boot.security.jwt.service;

import com.mgryshenko.samples.spring_boot.security.jwt.error.JwtValidationException;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtAccessDetails;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

public interface IJwtAccessDetailsService {
    IJwtAccessDetails create(String username, Collection<? extends GrantedAuthority> authorities);
    IJwtAccessDetails parse(String token) throws JwtValidationException;
}
