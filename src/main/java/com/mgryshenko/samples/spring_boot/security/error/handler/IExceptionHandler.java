package com.mgryshenko.samples.spring_boot.security.error.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IExceptionHandler<T> {
    void handle(T ex, HttpServletRequest request, HttpServletResponse response);
}