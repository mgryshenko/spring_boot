package com.mgryshenko.samples.spring_boot.security.error.model;

import java.util.Date;
import java.util.List;
import org.springframework.http.HttpStatus;

public interface IErrorData {
    HttpStatus getStatus();
    Date getTimestamp();
    String getPath();
    String getMessage();
    String getDebugMessage();
    List<ISubErrorData> getSubErrors();
}
