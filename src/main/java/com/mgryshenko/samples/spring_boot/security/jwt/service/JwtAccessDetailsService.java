package com.mgryshenko.samples.spring_boot.security.jwt.service;

import com.mgryshenko.samples.spring_boot.security.jwt.error.JwtValidationException;
import com.mgryshenko.samples.spring_boot.security.jwt.model.IJwtAccessDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@AllArgsConstructor
@Setter
public class JwtAccessDetailsService implements IJwtAccessDetailsService {

    private static final String CLAIMS_AUTHORITIES_NAME = "authorities";

    protected final int jwtExpirationSec;
    protected final String jwtSecret;
    protected final SignatureAlgorithm signatureAlgorithm;

    @Override
    public JwtAccessDetails create(String username, Collection<? extends GrantedAuthority> authorities) {
        JwtAccessDetails model = createDetails(username, authorities);
        model.setToken(createToken(model));
        return model;
    }

    private JwtAccessDetails createDetails(String username, Collection<? extends GrantedAuthority> authorities) {
        long now = System.currentTimeMillis();

        JwtAccessDetails model = new JwtAccessDetails();
        model.setEmail(username);
        model.setAuthorities(authorities);
        model.setIssuedDate(new Date());
        model.setExpiryDate(new Date(now + (jwtExpirationSec * 1000)));

        return model;
    }

    private String createToken(IJwtAccessDetails details) {
        List<String> authorityNames = details.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        return Jwts.builder()
                .setSubject(details.getEmail())
                .claim(CLAIMS_AUTHORITIES_NAME, authorityNames)
                .setIssuedAt(details.getIssuedDate())
                .setExpiration(details.getExpiryDate())
                .signWith(signatureAlgorithm, jwtSecret.getBytes())
                .compact();
    }

    @SuppressWarnings("unchecked")
    public JwtAccessDetails parse(String token) {
        Claims claims;
        List<String> authorityNames;
        Collection<? extends GrantedAuthority> authorities;

        try {
            claims = claims(token);
            authorityNames = (List<String>) claims.get(CLAIMS_AUTHORITIES_NAME);
            authorities = fromNames(authorityNames);
        } catch (Exception e) {
            throw new JwtValidationException(e);
        }

        JwtAccessDetails model = new JwtAccessDetails();
        model.setToken(token);
        model.setEmail(claims.getSubject());
        model.setAuthorities(authorities);
        model.setIssuedDate(claims.getIssuedAt());
        model.setExpiryDate(claims.getExpiration());
        return model;
    }

    private Collection<? extends GrantedAuthority> fromNames(Collection<String> authorities) {
        return authorities.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(collectingAndThen(toList(), Collections::unmodifiableList));
    }

    private Claims claims(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret.getBytes())
                .parseClaimsJws(token)
                .getBody();
    }

    @NoArgsConstructor
    @Getter
    @Setter
    private static class JwtAccessDetails implements IJwtAccessDetails {
        private String email;
        private Collection<? extends GrantedAuthority> authorities;

        private String token;
        private Date issuedDate;
        private Date expiryDate;
    }
}
