package com.mgryshenko.samples.spring_boot.security.oauth.model;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.Map;

public enum AuthProvider {
    local,
    facebook,
    google,
    github,
    unknown;

    private static final Map<String, AuthProvider> mappings = new HashMap<>(16);

    static {
        for (AuthProvider provider : values()) {
            mappings.put(provider.name(), provider);
        }
    }

    public boolean isLocal() {
        return this == local;
    }

    @NonNull
    public static AuthProvider fromName(@Nullable String method) {
        if (method == null) {
            return unknown;
        }
        return mappings.getOrDefault(method.toLowerCase(), unknown);
    }
}
