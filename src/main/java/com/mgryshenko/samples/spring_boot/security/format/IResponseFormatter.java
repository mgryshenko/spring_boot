package com.mgryshenko.samples.spring_boot.security.format;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

public interface IResponseFormatter {
    void addHeader(HttpServletResponse response, String name, String value);
    void addStatus(HttpServletResponse response, int status);
    void addJsonBody(HttpServletResponse response, Object content) throws IOException;
}
