package com.mgryshenko.samples.spring_boot.security.cors;

import java.net.URI;

public interface IAuthorizedOriginUriService {
    URI authorizeUri(String uri) throws UntrustedOriginUriException, MalformedUriException ;
}
