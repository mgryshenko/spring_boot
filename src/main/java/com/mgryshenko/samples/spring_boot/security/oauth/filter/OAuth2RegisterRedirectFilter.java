package com.mgryshenko.samples.spring_boot.security.oauth.filter;

import com.mgryshenko.samples.spring_boot.security.oauth.service.OAuth2HttpCookieResolver;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.mgryshenko.samples.spring_boot.security.oauth.filter.HttpCookieOAuth2AuthorizationRequestRepository.ORIGINAL_SERVLET_PATH_COOKIE;

public class OAuth2RegisterRedirectFilter extends OncePerRequestFilter {

    public static final int cookieExpireSeconds = 3 * 60;

    protected final OAuth2HttpCookieResolver cookieResolver = new OAuth2HttpCookieResolver();

    protected RequestMatcher callbackAuthenticationRequestMatcher = request -> false;
    protected RequestMatcher registerAuthenticationRequestMatcher = request -> false;
    protected Function<String, String> pathReplacer = Function.identity();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (isRegisterRequest(request)) {
            String originalPath = request.getServletPath();

            String originalPathEncoded = cookieResolver.base64encode(originalPath);
            cookieResolver.addCookie(response, ORIGINAL_SERVLET_PATH_COOKIE, originalPathEncoded, cookieExpireSeconds);

            String redirectPath = pathReplacer.apply(originalPath);
            request = new RegisterHttpServletRequestWrapper(request, redirectPath);
        } else if (!isCallbackRequest(request)) {
            cookieResolver.deleteCookie(request, response, ORIGINAL_SERVLET_PATH_COOKIE);
        }

        filterChain.doFilter(request, response);
    }

    protected boolean isCallbackRequest(HttpServletRequest request) {
        return callbackAuthenticationRequestMatcher.matches(request);
    }

    protected boolean isRegisterRequest(HttpServletRequest request) {
        return registerAuthenticationRequestMatcher.matches(request);
    }

    public void setCallbackRequestPath(String path, HttpMethod method) {
        callbackAuthenticationRequestMatcher = new AntPathRequestMatcher(path, method.name());;
    }

    public void setRegisterRequestPath(String redirectPathPattern, String authenticatePathPattern, HttpMethod method) {
        registerAuthenticationRequestMatcher = new AntPathRequestMatcher(redirectPathPattern, method.name());;
        pathReplacer = new PathReplacer(redirectPathPattern, authenticatePathPattern)::replace;
    }

    private static class PathReplacer {
        private final List<String> from;
        private final List<String> to;
        private final int chunksToReplace;

        public PathReplacer(String from, String to) {
            this.from = splitUriChunks(from);
            this.to = splitUriChunks(to);
            chunksToReplace = Math.min(this.from.size(), this.to.size());
        }

        public String replace(String path) {
            String result = path;
            for (int i = 0; i < chunksToReplace; i++) {
                result = result.replace(from.get(i), to.get(i));
            }
            return result;
        }

        private List<String> splitUriChunks(String uri) {
            List<String> list = new ArrayList<>();
            for (String chunk : uri.split("/")) {
                if (!StringUtils.isEmpty(chunk)) {
                    list.add(chunk);
                }
            }
            return list;
        }
    }
}
