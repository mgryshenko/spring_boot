package com.mgryshenko.samples.spring_boot.security.oauth.error;

import lombok.Getter;

@Getter
public class NoEmailInfoOAuth2Exception extends OAuth2ServiceException {

    private final String provider;

    public NoEmailInfoOAuth2Exception(String provider) {
        super("No email provided by \"" + provider + "\". Make sure your \"" + provider + "\" account has public access to email");
        this.provider = provider;
    }

    @Override
    public String getMsgKey() {
        return "error.auth.no_email_info";
    }

    @Override
    public String[] getMsgParams() {
        return new String[] {provider, provider};
    }
}
