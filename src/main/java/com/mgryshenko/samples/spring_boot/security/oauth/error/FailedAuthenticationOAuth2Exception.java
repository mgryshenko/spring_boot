package com.mgryshenko.samples.spring_boot.security.oauth.error;

public class FailedAuthenticationOAuth2Exception extends OAuth2ServiceException {

    private final static String MSG = "Couldn't proceed authentication";

    public FailedAuthenticationOAuth2Exception(Throwable t) {
        super(MSG, t);
    }

    @Override
    public String getMsgKey() {
        return "error.auth.failed_oauth";
    }
}
