package com.mgryshenko.samples.spring_boot.config;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.ArrayList;
import java.util.List;

import static com.mgryshenko.samples.spring_boot.config.WebSecurityConfig.PATH_ANY;

@Component
@ConfigurationProperties(prefix = "app.cors")
@Getter
public class CorsConfig {

    private final List<String> authorizedUris = new ArrayList<>();

    public static final class AuthorizedOrigins {
        private final List<String> authorizedUris = new ArrayList<>();

        public List<String> getAuthorizedUris() {
            return authorizedUris;
        }
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(authorizedUris);
        config.setAllowedMethods(List.of(CorsConfiguration.ALL));
        config.setAllowedHeaders(List.of(CorsConfiguration.ALL));
        config.setAllowCredentials(true);
        config.setMaxAge(60L * 60L);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration(PATH_ANY, config);
        return source;
    }
}
