package com.mgryshenko.samples.spring_boot.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@PropertySource(value = "classpath:application.properties")
@Getter
public class TaskConfig {

    @Value("${confirm_registration.expiration_sec}")
    private long confirmRegistrationExpirationSec;

    @Value("${confirm_reset_password.expiration_sec}")
    private long confirmResetPasswordExpirationSec;
}
