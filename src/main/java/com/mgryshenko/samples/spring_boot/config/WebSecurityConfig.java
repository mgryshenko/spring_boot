package com.mgryshenko.samples.spring_boot.config;

import com.mgryshenko.samples.spring_boot.security.cors.IAuthorizedOriginUriService;
import com.mgryshenko.samples.spring_boot.security.error.ErrorConfig;
import com.mgryshenko.samples.spring_boot.security.error.ErrorResponseConfigurer;
import com.mgryshenko.samples.spring_boot.security.jwt.JwtSecurityConfigurer;
import com.mgryshenko.samples.spring_boot.security.jwt.service.IJwtRefreshDetailsService;
import com.mgryshenko.samples.spring_boot.security.oauth.OAuthSecurityConfigurer;
import com.mgryshenko.samples.spring_boot.security.oauth.filter.HttpCookieOAuth2AuthorizationRequestRepository;
import com.mgryshenko.samples.spring_boot.security.oauth.service.IUserOAuth2DetailsService;
import com.mgryshenko.samples.spring_boot.service.Roles;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

import static org.springframework.http.HttpMethod.*;

@Slf4j
@Configuration
@ComponentScan(value = "com.mgryshenko.samples.spring_boot")
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String PATH_ANY = "/**";

    private final UserDetailsService userDetailsService;
    private final IUserOAuth2DetailsService userOAuth2DetailsService;
    private final IJwtRefreshDetailsService jwtRefreshService;
    private final IAuthorizedOriginUriService authorizedOriginUriService;
    private final ErrorConfig errorConfig;

    private ErrorResponseConfigurer errorResponseConfigurer;
    private JwtSecurityConfigurer jwtSecurityConfigurer;
    private OAuthSecurityConfigurer oAuthSecurityConfigurer;

    @Autowired
    public WebSecurityConfig(UserDetailsService userDetailsService,
                             IUserOAuth2DetailsService userOAuth2DetailsService,
                             IJwtRefreshDetailsService jwtRefreshService,
                             IAuthorizedOriginUriService authorizedOriginUriService,
                             ErrorConfig errorConfig) {
        this.userDetailsService = userDetailsService;
        this.userOAuth2DetailsService = userOAuth2DetailsService;
        this.jwtRefreshService = jwtRefreshService;
        this.authorizedOriginUriService = authorizedOriginUriService;
        this.errorConfig = errorConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/error").permitAll()
                .antMatchers(GET, "/auth/register/**").permitAll()
                .antMatchers(POST, "/auth/register").permitAll()
                .antMatchers(POST, "/auth/password/reset").permitAll()
                .antMatchers(POST, "/auth/password/save").permitAll()
                .antMatchers(POST, "/auth/password/change").authenticated()
                // ADMIN requests listed first
                .antMatchers(POST, "/admin/**").hasRole(Roles.ADMIN)
                .antMatchers(GET, "/admin/**").hasRole(Roles.ADMIN)
                .antMatchers(PATCH, "/admin/**").hasRole(Roles.ADMIN)
                .antMatchers(DELETE, "/admin/**").hasRole(Roles.ADMIN)
                // USER requests must be authenticated
                .antMatchers(DELETE, "/user/me").access("not( hasRole('ADMIN') ) and isAuthenticated()")
                .antMatchers(GET, PATH_ANY).authenticated()
                .antMatchers(POST, PATH_ANY).authenticated()
                .antMatchers(DELETE, PATH_ANY).authenticated()
                .antMatchers(PATCH, PATH_ANY).authenticated()
                // any other requests must be denied
            .anyRequest()
                .denyAll();

        errorResponseConfigurer
                .customize()
                    .addOAuth2RedirectStrategy(authorizedOriginUriService, "/oauth2/")
                    .apply(http);

        jwtSecurityConfigurer
                .customize(authenticationManager(), jwtRefreshService)
                    .login(POST, "/auth/login")
                        .and()
                    .refresh(POST, "/auth/token/refresh")
                        .and()
                    .failureHandler(errorConfig.noRedirectFailureHandler())
                    .apply(http);

        oAuthSecurityConfigurer
                .customize(authorizedOriginUriService, userOAuth2DetailsService)
                    .root()
                        .baseUri("/oauth2/**")
                        .and()
                    .registration()
                        .baseUri("/oauth2/register/*")
                        .and()
                    .authorization()
                        .baseUri("/oauth2/authorize")
                        .and()
                    .callback()
                        .baseUri("/oauth2/callback/*")
                        .and()
                    .failureHandler(errorConfig.noRedirectFailureHandler())
                    .apply(http);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Autowired(required = false)
    public void setJwtSecurityConfigurer(JwtSecurityConfigurer jwtSecurityConfigurer) {
        this.jwtSecurityConfigurer = jwtSecurityConfigurer;
    }

    @Autowired(required = false)
    public void setOAuthSecurityConfigurer(OAuthSecurityConfigurer oAuthSecurityConfigurer) {
        this.oAuthSecurityConfigurer = oAuthSecurityConfigurer;
    }

    @Autowired(required = false)
    public void setErrorResponseConfigurer(ErrorResponseConfigurer errorResponseConfigurer) {
        this.errorResponseConfigurer = errorResponseConfigurer;
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userDetailsService);
        auth.setPasswordEncoder(encoder());
        return auth;
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public AuthorizationRequestRepository<OAuth2AuthorizationRequest> cookieAuthorizationRequestRepository() {
        return new HttpCookieOAuth2AuthorizationRequestRepository();
    }
}
