package com.mgryshenko.samples.spring_boot.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = "classpath:secrets.properties")
@Getter
public class SecretsConfig {

    @Value("${secret.admin.name}")
    private String adminName;

    @Value("${secret.admin.email}")
    private String adminEmail;

    @Value("${secret.admin.password}")
    private String adminPassword;

}
