package com.mgryshenko.samples.spring_boot.web.controllers;

import com.mgryshenko.samples.spring_boot.persistence.model.UserAccount;
import com.mgryshenko.samples.spring_boot.security.jwt.model.EmailPasswordDto;
import com.mgryshenko.samples.spring_boot.web.dto.RegisterUserAccountDto;
import com.mgryshenko.samples.spring_boot.web.dto.UserAccountDto;
import debug.AbstractControllerTest;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class UsersControllerTest extends AbstractControllerTest {

    @Ignore
    @Test
    public void register() throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        RegisterUserAccountDto payload = new RegisterUserAccountDto();
        payload.setEmail("test_name@test.com");
        payload.setPassword("Pa$$w0rd");
        payload.setName("TestName");
        payload.setCompleteUri("http://localhost:8080");

        try {
            ResponseEntity<UserAccountDto> result = restTemplate.postForEntity(toUri("/auth/register"), payload, UserAccountDto.class);

            Assert.assertTrue(result.getStatusCode().is2xxSuccessful());
            Assert.assertNotNull(result.getBody());
            Assert.assertEquals(payload.getEmail(), result.getBody().getEmail());
            Assert.assertNull(result.getBody().getPassword());
        } catch (HttpClientErrorException e) {
            // If user is already exist
            Assert.assertEquals(e.getStatusCode(), HttpStatus.CONFLICT);
        }
    }

    @Ignore
    @Test
    public void login() throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        EmailPasswordDto payload = new EmailPasswordDto("test_name@test.com", "Pa$$w0rd");
        ResponseEntity<String> result = restTemplate.postForEntity(toUri("/auth/login"), payload, String.class);

        Assert.assertTrue(result.getStatusCode().is2xxSuccessful());
        Assert.assertNotNull(result.getBody());
        Assert.assertNotNull(result.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0));
    }

    @Ignore
    @Test
    public void userMe() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        addAuthorizationInterceptor(restTemplate);

        ResponseEntity<UserAccount> result = restTemplate.getForEntity(toUri("/user/me"), UserAccount.class);

        Assert.assertTrue(result.getStatusCode().is2xxSuccessful());
        Assert.assertNotNull(result.getBody());
    }

    @Ignore
    @Test
    public void deleteMe() throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        addAuthorizationInterceptor(restTemplate);

        try {
            restTemplate.delete(toUri("/user/me"));
        } catch (HttpClientErrorException e) {
            // If user doesn't exist
            Assert.assertEquals(e.getStatusCode(), HttpStatus.UNAUTHORIZED);
        }
    }
}