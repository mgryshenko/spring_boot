package com.mgryshenko.samples.spring_boot.web.controllers;

import debug.AbstractControllerTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class UserMeControllerTest extends AbstractControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(wac)
                .addFilter(springSecurityFilterChain)
                .build();
    }

    @Ignore
    @Test
    public void getUserMe() throws Exception {
        perform(get("/user/me")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + AUTHORIZATION_JWT_BEARER))
                .andExpect(status().isOk());
    }

    public <T extends MockHttpServletRequestBuilder> ResultActions perform(T requestBuilder) throws Exception {
        return mvc.perform(requestBuilder
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding(StandardCharsets.UTF_8.name()))
                .andExpect(this::logRequest)
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    protected void logRequest(MvcResult result) throws UnsupportedEncodingException {
        log.debug(result.getRequest().getMethod() + " : "
                + result.getRequest().getPathInfo() + " : "
                + requestContent(result) + " -> "
                + result.getResponse().getStatus() + " : "
                + result.getResponse().getContentAsString());
    }

    protected String requestContent(MvcResult result) {
        try {
            return result.getRequest().getContentAsString();
        } catch (Exception ignored) {}
        return "{}";
    }
}