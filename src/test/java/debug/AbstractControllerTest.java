package debug;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = "test")
public abstract class AbstractControllerTest {

    /** Base64 encoded JWT for user 'test_name@test.com', valid until 2025 June */
    public static final String AUTHORIZATION_JWT_BEARER = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0X25hbWVAdGVzdC5jb20iLCJhdXRob3JpdGllcyI6WyJSRUFEX1VTRVIiLCJXUklURV9VU0VSIiwiUk9MRV9VU0VSIl0sImlhdCI6MTU5MzUyNjE3NiwiZXhwIjoxNzUxMjA2MTc2fQ.PqV-uCgRRZXMrnWCpb6G4aokhxGnhRVlpSpVKrIZs7fWOltYIOtSDTsrs-wx0B6oaqeksSz0I_nVlqgO87UTTg";

    @LocalServerPort
    protected int randomServerPort;

    public URI toUri(String path) throws URISyntaxException {
        return new URI("http://localhost:" + randomServerPort + path);
    }

    public void addAuthorizationInterceptor(RestTemplate restTemplate) {
        restTemplate.getInterceptors().add((request, body, execution) -> {
            HttpHeaders headers = request.getHeaders();
            if (!headers.containsKey(HttpHeaders.AUTHORIZATION)) {
                headers.setBearerAuth(AUTHORIZATION_JWT_BEARER);
            }
            return execution.execute(request, body);
        });
    }

    public void setNoFollowRedirect(RestTemplate restTemplate) {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory() {
            @Override
            protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
                super.prepareConnection(connection, httpMethod);
                connection.setInstanceFollowRedirects(false);
            }
        };
        restTemplate.setRequestFactory(factory);
    }
}
